import re
import os
import pandas as pd
import itertools
import numpy as np
import statistics as s
import string
#import pyparsing as pp
from pyparsing import Word, alphas, nums, WordStart, WordEnd, Literal, Optional, SkipTo, ParseResults, CaselessLiteral,\
        OneOrMore, Group, White, ZeroOrMore, NotAny, Suppress, oneOf, Combine, Regex, lineStart, Empty, locatedExpr, ParseException, LineStart

"""
Todo:
   - road_section have to match: "de brug naar de tunnel Amsterdam". It does not match now.
   - Load learned place names. Put an option for using a pre-constructed vs. constructing a new place name list.
   - put basic match: if nothing matches, match ... (that last attempt will target small pieces of information which do not have a context to match properly.)
   Be careful, do not much everything.
   - Create a list that should be skipped: goedemorgen, <links>, etc. Include just expressions that may interfere with other valuable information.
   - update the place name list automatically, Store the place name list at the end of the analysis. The file name should be encoded with additional version number.
   - Added location names can be highlighted in the log file or at least on the screen.
   - include river names, park areas to the construct_location_grammar()
   - insert negatives, not allowed ones, for the place name learning: "#ehv"?
   - call learn_place_names() every time you get a new set of tweets as input. It will update the place list. So that will be a central place to improve this module.
   - # add *controle variations to construct_monitoring_grammar() Check all notes for * starting tokens in the code.
   - collective timexes, place names: peaking together? This will give related ones in terms of semantics or ortography.

   - Just timex or just place name should match at the very end. As defaults. At a higher rank, they should match with some logic. 
      For example for time expressions: (start points, end points, periods, exact points.)

   - Each domain should have its own class, maybe set of classes under an abstract class. For example Traffic! Consider that when you have more than 1 domain.
     Each information type can be a class.
   - gladheid? weather condition? 

Timex to do:
    - timex pyparsing module, should be additional to the list. It will be before the list since it will try to cover long expressions.
    - there are many code/grammar under "Time Expression Recognition" (traffic_events.ipynb)
    - define basic date:
        1) vanaf|op|.. date

    - define basic time
        1) vanaf|tot|.. time (I put them as optionals before a date.)

    - define basic weekday:
         1) volgende|aanstande|.. weekday

    - define day time:
      ...

    - above mentioned rules should be applied before the list!
    - Learn time expressions!

It should match, check:
    - van ' s-Hertogenbosch -> Utrecht
    - vast op de #A2

"""

class Sinfex():

	def __init__(self, place_names_file="placenames.txt", geonames_file="NL_Geonames/NL.txt", timex_file="timexall11.txt"): # /Users/alihurriyetoglu/Dropbox/Projects/sinfex/
		self.script_dir = os.path.dirname(__file__)+"/"

		self.place_names_file = self.script_dir+place_names_file
		self.geonames_file = self.script_dir+geonames_file
		self.timex_file = self.script_dir + timex_file

		self.tssnLit = self.get_caseless_word_literal("tussen") # add variations of 'tussen' for it
		self.enLit = self.get_caseless_word_literal("en")
		self.vanLit = self.get_caseless_word_literal("van") # add variations of 'tussen' for it
		self.naarLit = WordStart() + CaselessLiteral("naar") + WordEnd() # add variations of 'tussen' for it
		self.totLit = WordStart() + CaselessLiteral("tot") + WordEnd() # add variations of 'tussen' for it
		self.totAanLit = WordStart() + CaselessLiteral('tot aan') + WordEnd() # add variations of 'tussen' for it
		self.vanafLit = WordStart() + CaselessLiteral('vanaf') + WordEnd() # add variations of 'tussen' for it
		self.naLit = WordStart() + CaselessLiteral('na') + WordEnd()
		self.voorLit = WordStart() + CaselessLiteral('voor') + WordEnd()
		self.bijLit = WordStart() + oneOf(["dichtbij","vlakbij","bij"], caseless=True) + WordEnd() # at
		self.inLit = WordStart() + CaselessLiteral("in") + WordEnd() # at
		self.thvLit = self.get_caseless_word_literal(['ter hoogte van','thv','t.h.v']) # at
		self.metLit = WordStart() + CaselessLiteral("met") + WordEnd()
		self.vanuitLit = WordStart() + CaselessLiteral("vanuit") + WordEnd()
		self.plusLit = WordStart() + CaselessLiteral('plus') + WordEnd()
		self.vanwegeLit = WordStart() + CaselessLiteral('vanwege') + WordEnd()
		self.ivmLit = WordStart() +oneOf(["in verband met","ivm","i.v.m."], caseless=True) + WordEnd()
		self.doorLit = WordStart() + CaselessLiteral('door') + WordEnd()
		self.tgvLit = WordStart() +oneOf(["ten gevolge van","tgv","t.g.v."], caseless=True) + WordEnd()
		self.alsgevolgvanLit = WordStart() +oneOf(["als gevolg van"], caseless=True) + WordEnd()
		self.metalsgevolgLit = WordStart() +oneOf(["met als gevolg"], caseless=True) + WordEnd()
		self.tijdensLit = WordStart() + CaselessLiteral('tijdens') + WordEnd()
		self.opLit = WordStart() + CaselessLiteral('op') + WordEnd()
		self.langsLit = WordStart() + CaselessLiteral('langs') + WordEnd()
		self.perLit = WordStart() + CaselessLiteral('per') + WordEnd()
		self.nabijLit = WordStart() + CaselessLiteral('nabij') + WordEnd()
		self.vlakLit = WordStart() + CaselessLiteral("vlak voor") + WordEnd()
		self.opzijvanLit = WordStart() + CaselessLiteral('opzij van') + WordEnd()
		self.naastLit = WordStart() + oneOf(["vlak naast","naast"], caseless=True) + WordEnd()
		self.tennoordenvanLit = WordStart() + (CaselessLiteral("aan de noordkant van") | CaselessLiteral('ten noorden van')) + WordEnd()
		self.tenoostenvanLit = WordStart() +(CaselessLiteral("aan de oostkant van") | CaselessLiteral('ten oosten van')) + WordEnd()
		self.tenwestenvanLit = WordStart() + (CaselessLiteral("aan de westkant van") | CaselessLiteral('ten westen van')) + WordEnd()
		self.tenzuidenvanLit = WordStart() +(CaselessLiteral("aan de zuidkant van") | Literal('ten zuiden van')) + WordEnd()
		self.binnenLit = self.get_caseless_word_literal("binnen")
		self.elkeLit = self.get_caseless_word_literal(["elke","elk"])
		self.iedereLit = self.get_caseless_word_literal(["iedere","ieder"])

		self.sm_token = WordStart()+Combine(Optional('#')+Word(alphas))+WordEnd()


		#this_dir = os.path.dirname(__file__) 

		# write a different init for the place names.
		learned_wrongly = ["een Smart","een auto","een personenauto","een truck","een vrachtwagen","personenauto","oost","dag","auto","burgers"]
		self.placelist = [l.strip() for l in open(self.place_names_file) if len(l.strip())>0 and l.strip() not in learned_wrongly]

		adhoc_placelist = ["A'dam", "Amers ' f", "Amers'f", "' t Harde", "'t Harde"]

		self.placelist = list(set(self.load_geonames(self.geonames_file) + self.placelist)) # sort it by length if pyparsing does not take care of longest firts match first automatically.

		ambiguous_place = ["Brand","Gem","Wel","Een","Zuid","Noord","Oost","West","Slot","Straat","Onder","Die","Keer","Vrij","Hinder","Waar","Travel","Haar","Station"]
		self.placelist = [p for p in self.placelist if p not in ambiguous_place]
		#self.placelist = [p for p in self.placelist if p != "brand"]
		self.placelist += adhoc_placelist

		print("length of the initial place names list:",len(self.placelist))

		self.token_road_list = ["autobahn","autobaan","autosnelweg","autosnelwegen","autostrada","autoweg","doodlopende weg","doorgaande weg","hoofdweg", \
				"parallel weg","randweg","rijksweg","ring","ringweg","sluipweg","snelweg",
				"autobahnen","autobanen","autostrada's","autowegen","doodlopende wegen","doorgaande wegen","hoofdwegen",\
                "parallelweg","parallelwegen","randwegen","rijkswegen","ringen","ringwegen","sluipwegen","snelwegen",\
                "stadstraverse","stadstraverses","verbindingsweg","verbindingswegen","zijstraat","zijstraten", "zijweg", "zijwegen"]

		self.token_infra1 = WordStart()+oneOf(["tunnel","knooppunt","knp","knp.","knpt","kp","knp.","kpt","kpt.","afrit",\
		                                 "aansluiting","plein","rotonde","afslag","aquaduct","boogbrug","brug","draaibrug",\
		                                "ecoduct","fietspad","fietstunnel","fietsbrug","flyover","hangbrug",\
		                                "klaverblad","kruispunt","loopbrug","oprit","pontonbrug", \
		                                 "verkeersknooppunt","verkeersplein","verkeerstunnel","viaduct"\
		                                 "tunnels","knooppunten","afritten","op- en afritten", "bruggen",\
		                                 "viaducten","fietspaden","rotondes","pleinen"], caseless=True)+WordEnd()

		self.place_names = self.construct_regex_based_placelist_grammar(self.placelist)

		self.adhoc_tmx = ["binnen half uur","dit moment","halve minuut","een jaartje","half uur"] # missing important timexes.

		self.not_place_name_list = ['mij','hij','zij','wij','ze','jullie','ik']

		self.ignore_tmx = ['weer','volgende']

		self.get_tmx_list(filename=self.timex_file) # load the default one.

		# http://pyparsing.wikispaces.com/share/view/6031101
		# define an expression that always matches, and simply reports its location
		self.endLocnMarker = Empty().setParseAction(lambda s,loc,t: loc)

		self.opt_punct = Optional(OneOrMore(oneOf(list(string.punctuation)))).suppress()

	# http://pyparsing.wikispaces.com/share/view/6031101
	# create a helper method that appends the end marker to an expression, 
	# and gives it a results name to make it easy to get
	def exprWithEndLocn(expr):
		def cleanupTokens(s,l,t):
			del t[-1:]
			t["startOfTokens"] = l
		return ((expr+endLocnMarker("endOfTokens")).setParseAction(cleanupTokens))
 

	
	def load_geonames(self, geonamesfile):
		nl_places = pd.read_table(geonamesfile,names=["geonameid","pname","asciiname","alternatenames",\
		        "latitude","longitude","feature_class","feature_code","country_code","cc2","admin1_code","admin2_code","admin3_code",\
		        "admin4_code","population","elevation","dem","time_zone","modification_date"], index_col=False)

		nl_places = nl_places[nl_places.time_zone == "Europe/Amsterdam"]

		def merge_names(n):
			alt_names = []
			if n.alternatenames is not None and n.alternatenames is not np.nan:
				alt_names = n.alternatenames.split(',')
			return list(set([n.pname]+[n.asciiname]+alt_names))
		 
		nl_places["all_names"] = nl_places.apply(merge_names, axis=1)

		all_pnames_list = list(itertools.chain.from_iterable(nl_places.all_names.values))

		all_pnames_list = list(set(all_pnames_list))

		all_pnames_list = [p for p in all_pnames_list if ("\\" not in p) and ("*" not in p)]

		all_pnames_list = [pn for pn in all_pnames_list if len(pn)>2]

		all_pnames_list = [p for p in all_pnames_list if p not in ["brand","wel","gem"]]

		print("Number of place names from geonames:",len(all_pnames_list))

		return all_pnames_list

	def getlinelist(self,filename, striplines=True):
		with open(filename) as f:
			linelist = f.read().splitlines()
	    
		if striplines:
			linelist = [l.strip() for l in linelist]
	        
		return [l for l in linelist if len(l)>0] # Exclude blank lines!

	def get_tmx_list(self, filename="timexall11.txt"):
		print(filename)
		use_wlistT = self.getlinelist(filename)

		mean_tmx_len = s.mean([len(t) for t in use_wlistT])
		print("mean timex length:",mean_tmx_len)
		use_wlistT_short = [t for t in use_wlistT if len(t)<mean_tmx_len] # use just the short ones.
		print("Length of short tmx list:",len(use_wlistT_short))

		use_wlistT_short = [t for t in use_wlistT_short if t not in self.ignore_tmx]

		self.timex_list = use_wlistT_short + self.adhoc_tmx

		print("Length of the final tmx list:",len(self.timex_list))


	def set_timex_list(self, timexlist=None):

		self.timex_list = timexlist + self.adhoc_tmx
		self.timex_list = [t for t in self.timex_list if t not in self.ignore_tmx]

	def get_caseless_word_literal(self, word):
		if isinstance(word, str):
			return WordStart() + CaselessLiteral(word) + WordEnd()
		elif isinstance(word, list):
			return WordStart() +oneOf(word, caseless=True) + WordEnd()
		else:
			raise Exception("Literals should be type of strings or list of the strings. Given type:" + type(word))


	def extract_timex_listbased(self, timexlist, textseries):
		
		wlist_ptrn = self.get_regex_length_sorted_list(timexlist)

		return textseries.apply(lambda twtxt: re.sub(wlist_ptrn,lambda m: "<timex> "+m.group(0)+" </timex>", twtxt))

	def get_regex_str_from_list(self, mylist):
		"Sentence boundary control with \\b does not allow place names to start with apostrophe. Therefore, we do not use them together."
		rgx_str = "\\b"
		for i,item in enumerate(mylist):
			if item[0] == "'":
				rgx_str += "\\b|"+item
			else:
				if i == 0: # do not allow duplicate
					rgx_str += item
				else:
					rgx_str += "\\b|\\b"+item

		rgx_str += "\\b"
		return rgx_str


	def get_regex_length_sorted_list(self,mylist):
		use_wlist = list(set(mylist))
		use_wlist = sorted(use_wlist, key=len, reverse=True)
		use_wlist = [tx.replace('.','\.') for tx in use_wlist] # not to match anything with . (dot)
		use_wlist = [tx.replace("(","\(") for tx in use_wlist] # not to match anything with . (dot)
		use_wlist = [tx.replace(")","\)") for tx in use_wlist] # not to match anything with . (dot)

		return re.compile(self.get_regex_str_from_list(use_wlist), re.I)


	def construct_regex_based_placelist_grammar(self, plc_list):
		wlist_ptrn = self.get_regex_length_sorted_list(plc_list)

		cardinal_points = WordStart()+oneOf(["zuid","noord","oost","west"], caseless=True)+WordEnd()
		
		#plc_grmmr = Combine(Optional(Word("#")|infrastructure)+Regex(wlist_ptrn))
		return Optional(self.token_infra1|cardinal_points) + Combine(Optional(Word("#"))+Regex(wlist_ptrn)+Optional(White()+CaselessLiteral("centrum"))+Optional(Literal("-")+oneOf(["zuid","noord","oost","west"], caseless=True)))

	def construct_regex_based_timex_grammar(self):
		
		wlist_ptrn = self.get_regex_length_sorted_list(self.timex_list)

		def timex_trnsfrm(loc, toks):
		    return {'start':loc,'end':loc+len(" ".join(toks)), 'type':'timex', 'timex':" ".join(toks), 'tokens':" ".join(toks)}

		tmx_grmmr = Optional(self.totLit|self.vanafLit|self.binnenLit|self.elkeLit|self.iedereLit) + Regex(wlist_ptrn)
		tmx_grmmr.setParseAction(timex_trnsfrm)

		return tmx_grmmr

	def learn_place_names(self, training_tweets):
		# - tussen p1 en p2 : 
		#    - list of p1 should be used to create the options of the p2 with oneOf
		#    - p1 should be updated if we come across a new variation as we run the 2. pass
		#    Update p1 list for every new tweet: if p2 is in the current p1 list, the new observed p1 is a place name!
		# - how to make '#' optional and supress it.
		# - check p2 in a case insensitive manner

		print("len of the training tweets:",len(training_tweets))

		plc_word = Optional('#') + Word(alphas+"'.-") # 
		plcnm_d1 = Group(OneOrMore(~self.enLit+plc_word))
		plcnm_d2 = Group(OneOrMore(~self.naarLit+plc_word))
		plcnm_d3 = Group(OneOrMore(~self.totLit+plc_word))

		plcnm_d1.setParseAction(lambda pnamelist: " ".join(pnamelist[0]))
		plcnm_d2.setParseAction(lambda pnamelist: " ".join(pnamelist[0]))
		plcnm_d3.setParseAction(lambda pnamelist: " ".join(pnamelist[0]))

		tssn_place_en = self.tssnLit+plcnm_d1.setResultsName("place")+self.enLit
		van_place_naar = self.vanLit+plcnm_d2.setResultsName("place")+self.naarLit
		van_place_tot = self.vanLit+plcnm_d3.setResultsName("place")+self.totLit

		article_infrastructure = Optional(~self.token_infra1+self.sm_token) + infrastructure

		#version 2
		# PASS 1: identify place names.
		tssn_optn_infra_place_en = self.tssnLit+Optional(article_infrastructure).setResultsName("infrastructure")+plcnm_d1.setResultsName("place")+self.enLit 
		tssn_optn_infra_place_en.setParseAction(lambda toks: toks["place"])

		plc_list = training_tweets.apply(lambda tw: (tw, tssn_optn_infra_place_en.searchString(tw)))
		learned_places = [(t[0],"PLACE:"+t[1][0][0]) for t in plc_list[:1000] if t[1]] # output: (tweet, location)

		learned_places = [t[1][0][0] for t in plc_list if t[1]] # output: (tweet, location)

		print("length of the learned places:",len(learned_places))

		added_place_names = list(set(learned_places) - set(self.placelist))
		print("length of the to be added place name set",len(added_place_names))
		added_place_names = [apn for apn in added_place_names if apn not in self.not_place_name_list]


		self.update_place_name_list_grammar(added_place_names)
		
		return added_place_names

	def update_place_name_list_grammar(self, add_place_names):
		
		self.placelist += add_place_names
		#self.place_names = WordStart() + oneOf(self.placelist, caseless=True) + WordEnd()
		self.place_names = oneOf(self.placelist, caseless=True)

		with open(self.place_names_file, 'w') as fw:
			for p in sorted(self.placelist):
				fw.write(p+"\n")

			print("updated the place list with:", add_place_names)


	def construct_traffic_event_grammar(self):
		# define: *(wagen|trailer|bus|auto)
		# define different "makes of cars"/actors etc.
		# define *pech
		token_roadworks1 = oneOf(["asfalteringswerkzaamheden","bouw- en onderhoudsprojekten",
		                   "bouw- en onderhoudsprojecten","herstelwerkzaamheden","onderhoudswerkzaamheden",
		                  "reparatiewerkzaamheden","wegwerkzaamheden","werkzaamheden","onderhoud"],caseless=True)

		#pattern_roadworks = Optional(~token_roadworks1) + Combine(Word(alphas)+oneOf(["onderhoud","werkzaamheden"], caseless=True))

		# define: *onderhoud, *werkzaamheden
		pattern_roadworks = Combine(Optional(Literal("#"))+Word(alphas, exact=1)+SkipTo(oneOf(["werkzaamheden","onderhoud"], caseless=True), include=True, failOn=White()))

		token_roadworks = token_roadworks1 | pattern_roadworks

		# removed intentionally:  "file"
		events1 = oneOf(["autobrand","berging","bergingsactiviteit","gaslek","lekkage","pechgeval" \
		                "opruimingswerkzaamheden","kijkersfile","problemen","rookontwikkeling","olielekkage","autobranden", \
		                "bergingsactiviteiten","pechgevallen","probleem","spooronderzoek","voertuigbrand","voertuigbranden"],caseless=True)

		token_adj = oneOf(["brandende","gekantelde","geschaarde","gestrande","gevallen","kapotte","loslopend",\
		                  "loslopende","omgevallen","overstekend","uitgebrande","verloren","verschoven","defecte","geslipte",
		                   "rondslingerende","slingerende","van de weg geraakte","vastgelopen"])
		token_vehicle = oneOf(["aanhanger","aanhangwagen","aanhangwagentje","ambulance","auto","auto met caravan",\
		                      "autobus","autootje","busje","cabriolet","camper","caravan","dieplader","dubbeldekker",
		                      "fiets","jeep","koets","kraanwagen","lesauto","lijkwagen","lijnbus","motor","mpv",\
		                      "oldtimer","oplegger","pendelbus","pendelbusje","personenauto","personenbus","personenbusje",
		                      "personenwagen","politieauto","scooter","sleepwagen","sneeuwploeg","sneeuwschuiver",\
		                      "sportauto","sportwagen","tankwagen","takelwagen","taxi","terreinwagen","traktor",
		                       "tractor","trailer","paardentrailer","transporter","transportbus","trekker","truck",
		                      "vervoermiddel","vervoersmiddel","voertuig","voertuigen","vouwcaravan","vouwfiets",
		                       "vouwwagen","vrachtauto","vrachtwagen","wagentje","ziekenwagen","zijspan","brandweerauto",
		                      "brandweerwagen","aanhangers","aanhangwagens","aanhangwagentjes","ambulances","auto's","auto's met caravan",\
		                      "autobussen","autootjes","busjes","cabriolets","campers","caravans","diepladers","dubbeldekkers",\
		                      "fietsen","jeeps","koetsen","kraanwagens","lesauto's","lijkwagens","lijnbussen","motoren","mpv's",\
		                      "oldtimers","opleggers","pendelbussen","pendelbusjes","personenauto\’s","personenbussen","personenbusjes",\
		                      "personenwagens","politieauto's","scooters","sleepwagens","sneeuwploegen","sneeuwschuivers",\
		                      "sportauto's","sportwagens","tankwagens","takelwagens","taxi's","terreinwagens","traktoren",\
		                      "tractoren","trailers","paardentrailers","transporters","transportbussen","trekkers","trucks",\
		                      "vervoermiddelen","vervoersmiddelen","vouwcaravans","vouwfietsen","vouwfietsje", "vouwfietsjes",\
		                      "vouwwagens","vrachtauto's","vrachtwagens","wagentjes","ziekenwagens", "ziekenauto", "ziekenauto's",\
		                      "zijspannen","brandweerauto's","motor met zijspan","motoren met zijspan","streekbus","streekbussen","stadsbus","stadsbussen",\
		                      "bolide","bolides","leasebak","lease bak","leasebakken","lease auto","lease auto's","cabrio","cabrio's",\
		                      "bedrijfauto","bedrijfswagen","bedrijfsauto's","bedrijfswagens"],caseless=True)

		token_vehicle_brand = oneOf(["ALFA ROMEO","AUDI","BMW","CADILLAC","CHEVROLET","CHRYSLER","CITROEN","DACIA","DAIHATSU", "DODGE,DONKERVOORT", "DATSUN", "FARRARI", "FIAT",
		 "FORD", "FISKER", "HONDA", "HUNDAI", "JAGUAR", "JEEP", "KIA", "LADA", "LAMBORGINI", "LANCIA", "LAND ROVER", "LANDWIND", "LEXUS", "MASERATI,MAZDA", "MERCEDES", "MERCEDES-BENZ", 
		 "MITSUBISHI", "MCLAREN", "NISSAN", "OPEL", "PEUGEOT", "PORSCHE", "QOROS", "RENAULT", "ROLS-ROYCE", "SAAB", "SKODA", "SSANGYONG", "SUBARU","SUZUKI", "TOYOTA", "VOLKSWAGEN", "VOLVO"], caseless=True)
		 

		events2 = token_adj + token_vehicle

		token_object = oneOf(["boom","koe","koeien","lading","obstakel","varkens","vee","wild","paard","paarden","schaap","schapen","bomen","obstakels","zwaan",\
			                "zwanen","gans","ganzen","geit","geiten","kip","kippen","tak","takken"], caseless=True)
		events3 = token_adj + token_object

		token_mishap1 = oneOf(["bandenpech","klapband","lekke band","motorpech","pech"]) 
		pattern_mishap =  Combine(Optional(Literal("#"))+Word(alphas, exact=1)+SkipTo(oneOf(["pech"], caseless=True), include=True, failOn=White()))

		token_mishap = token_mishap1 | pattern_mishap

		events4 = token_vehicle + self.metLit + Optional(~token_mishap + self.sm_token) + token_mishap

		token_plain_event = events1 | events2 | events3 | events4

		token_accident = oneOf(["aanrijding","botsing","auto-ongeluk","busongeluk","frontale aanrijding","frontale botsing",
		                       "kettingbotsing","kopstaartbotsing","kop-staartbotsing","ongeval","verkeersongeluk",
		                        "verkeersongeval","pechgeval","aanrijdingen","botsingen","verkeersongelukken","pechgevallen",
		                        "slippartij","slippartijen","verkeersongevallen", "kop-staartbotsingen"],caseless=True)

		token_accident2 = oneOf(["ongeluk"],caseless=True)


		token_participant = oneOf(["automobilist","automobiliste","automobilisten","beroepschauffeur","bestuurder",\
		                    "buschauffeur","bijrijder","bromfietser","chauffeur","crosser","fietsster","fietser","snorfietser","snorfietsster","inzittende",\
		                     "inzittenden","motorrijder","passagier","tegenligger","tegenliggers","trucker","voetganger",\
		                     "weggebruiker","weggebruikers","automobilistes","beroepschauffeurs","bestuurders","lifter","lifters",\
		                     "buschauffeurs","bijrijders","bromfietsers","chauffeurs","crossers","fietsers","scooterrijdster",\
		                     "motorrijders","passagiers","tegenliggers","truckers","voetgangers","automobilistes","beroepschauffeurs","bestuurders","lifter","lifters"\
"buschauffeurs","bijrijders","bromfietsers","chauffeurs","crossers","fietsers","motorrijders","passagiers","tegenliggers","truckers","voetgangers","spookrijder","spookrijdster"], caseless=True)

		actor_pattern1 = self.metLit + Optional(~(token_vehicle|token_participant|token_vehicle_brand)+Word(alphas+nums)) + (token_participant|token_vehicle|token_vehicle_brand)
		actor_pattern2 = self.tssnLit + Optional(~(token_vehicle|token_participant|token_vehicle_brand)+Word(alphas+nums)) + (token_participant|token_vehicle|token_vehicle_brand) + self.enLit + \
		               Optional(~(token_vehicle|token_participant|token_vehicle_brand)+Word(alphas+nums)) + (token_participant|token_vehicle|token_vehicle_brand)
		
		actor_pattern = actor_pattern1 | actor_pattern2

		# Events
		observed_event1 = (self.vanwegeLit|self.ivmLit|self.doorLit|self.tgvLit|self.alsgevolgvanLit|self.metalsgevolgLit|self.naLit) \
		                    + Optional(~token_roadworks+self.sm_token) + token_roadworks
		    
		observed_event2 = (self.vanwegeLit|self.ivmLit|self.doorLit|self.tgvLit|self.alsgevolgvanLit|self.metalsgevolgLit|self.naLit) \
		                    + Optional(~token_plain_event+self.sm_token) + token_plain_event
		    
		observed_event3 = Optional(self.vanwegeLit|self.ivmLit|self.doorLit|self.tgvLit|self.alsgevolgvanLit|self.metalsgevolgLit|self.naLit) + Optional(~token_accident+self.sm_token) + token_accident + Optional(actor_pattern)

		observed_event3_2 = (self.vanwegeLit|self.ivmLit|self.doorLit|self.tgvLit|self.alsgevolgvanLit|self.metalsgevolgLit|self.naLit) \
		                    + Optional(~token_accident2+self.sm_token) + token_accident2 + Optional(actor_pattern)

		observed_event3_3 = (lineStart|Optional(~self.perLit + self.sm_token)) + token_accident2
		    
		observed_event4 = token_plain_event | token_roadworks + token_accident | token_roadworks + token_accident2
    
		def observed_event_trnsfrm(loc, toks):
		    return {'start':loc, 'end':loc+len(" ".join(toks)), 'type':'alert', 'alert':" ".join(toks), 'tokens':" ".join(toks)}
    
		observed_event = observed_event3 | observed_event3_2 | observed_event1 | observed_event2 | observed_event3_3 | observed_event4 # Order matters!
		observed_event.setParseAction(observed_event_trnsfrm)

		return observed_event

	def construct_road_pattern1(self):
		a_n_roads = WordStart()+Combine(Optional(Literal("#"))+Word("ANan",exact=1)+Word(nums,exact=1, excludeChars='0')+Optional(Word(nums, min=1, max=2)))+WordEnd()
		s_roads = WordStart()+Combine(Optional(Literal("#"))+CaselessLiteral("s")+oneOf(["100","101","102","103","104","105","106","107","108","109","110","111","112","113",\
			"114","115","116","117","118","119","120","121","122","123","124","125","126","127","150","151","152","153","154","155","200"]))+WordEnd()

		return a_n_roads | s_roads

	def construct_road_pattern2(self):
		return WordStart()+Combine(Word(alphas+".-")+oneOf(["weg","straat","straatweg","laan","dijk","autoweg","ring","randweg","rondweg","traverse","dreef"], caseless=True))+WordEnd()

	def construct_token_road(self):

		token_road1 = WordStart()+oneOf(self.token_road_list, caseless=True)+self.opt_punct+WordEnd()
		#token_roadx = oneOf(["weg","route","straat","laan"], caseless=True)+WordEnd()
		
		def validateString(tokens):
			#if tokens[0]
			not_road_id = set(self.token_road_list + ["terugweg","vanweg","beweg","heenweg","pakweg","terweg","onderweg","nieuweg","slaan","ringen","wasstraat",\
			                        "kapotslaan","omslaan","overslaan","ontslaan","massabeweg","opslaan","amvb-route","kabelaan","voorwielaan",\
			                        "autowasstraat","schijnbeweg","klimweg","naslaan","verweg","melkweg","lichaamsbeweg","milaan","terugslaan","openslaan","uitslaan",\
			                        "aanslaan","dichtslaan","doorslaan","afslaan","waterweg","inslaan","verslaan","vlaan","ffweg","oost-Vlaan"])
			mytoks = tokens[0].lower().strip("#")
			if (mytoks in not_road_id) and ("=" not in mytoks) and ("..." not in mytoks) and ("://" not in mytoks):
				raise ParseException("Invalid date string (%s)" % mytoks)

			
			#raise ParseException("Invalid date string (%s)" % tokens[0])
		#date.setParseAction(validateDateString)

		token_road_weg = Combine(Optional(Literal("#"))+Word(alphas, exact=1)+SkipTo(oneOf(["weg"], caseless=True), include=True, failOn=WordEnd()))#.setParseAction(validateWegString)
		token_roadx = Combine(Optional(Literal("#"))+Word(alphas, exact=1)+SkipTo(oneOf(["weg","route","straat","laan"], caseless=True), include=True, failOn=White())).setParseAction(validateString)
		token_road = token_road1 | token_roadx #| token_road_weg

		return token_road


	def construct_road_id_grammar(self):

		# WHYY:  ('Onderweg naar Nieuwjaarsborrel . Ben benieuwd . Rustig op de snelweg .  #a2 richting den bosch  #a15 richting Tiel',
		#  ([(['op', 'de', 'snelweg'], {})], {})) # it should match single #a15
		road_pattern1 = self.construct_road_pattern1()
		token_road = self.construct_token_road()

		def road_pattern1x_trnsfrm(loc,toks):
			return {'start':loc,'end':loc+len(" ".join(toks)),'type':'road_id','road_id':toks[0], 'tokens':toks[0]}

		road_pattern1x = self.construct_road_pattern1()
		road_pattern1x.setParseAction(road_pattern1x_trnsfrm)
		#road_pattern1 = road_pattern1.setResultsName("road_pattern1")

		# def road_pttrn1_trnsfrm(loc,toks):
		#     return {'start':loc,'type':'road_pattern1','road_pattern1':toks["road_pattern1"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		# road_pattern1.setParseAction(road_pttrn1_trnsfrm)
		#............................................................................
		# No match! Does it work?
		road_pattern2 = self.construct_road_pattern2()
		#road_pattern2 = road_pattern2.setResultsName("road_pattern2")

		# def road_pttrn2_trnsfrm(loc,toks):
		#     return {'start':loc,'type':'road_pattern2','road_pattern2':toks["road_pattern2"], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		# road_pattern2.setParseAction(road_pttrn2_trnsfrm)
		#............................................................................

		#road_pattern = road_pattern1 | road_pattern2
		

		#**************DANGER ZONE*****************#
		token_road2 = WordStart()+Combine(Optional(Literal("#"))+oneOf(self.token_road_list, caseless=True))+WordEnd()

		token_road2 = token_road2.setResultsName("token_road")

		def tok_road2_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_id','road_id':toks["token_road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		token_road2.setParseAction(tok_road2_trnsfrm)
		#............................................................................
		road_identifiers = Group((road_pattern1 | road_pattern2 | token_road)).setResultsName("road_identifier")
		road_id1 = (self.opLit|self.nabijLit|self.langsLit|self.bijLit|self.vlakLit|self.naastLit)("prep") + Optional(~road_identifiers + self.sm_token)("anyword") + road_identifiers
		
		road_id1 = locatedExpr(road_id1)
		road_id1 = road_id1.setResultsName("road_id")

		def road_id_trnsfrm(s, loc, toks):
		    #return (type(toks["road_id"]),[type(m) for m in toks["road_id"]], toks["road_id"])
		    # m_tokens = " ".join([t if isinstance(t,str) else t[0] for t in toks])
		    # r_id = toks["road_id"]["road_identifier"][0]
		    tdict = toks.asDict()["road_id"]
		    return {'start':loc,'type':'road_id',"tokens":s[tdict["locn_start"]:tdict["locn_end"]], 'road_id':tdict["road_identifier"][0], 'end':tdict["locn_end"]}
		    
		road_id1 = road_id1.setParseAction(road_id_trnsfrm)
		
		road_id =  road_id1 | token_road2 | road_pattern1x # Ali added "| road_condit_pattern1" it to match single: #A2, #N9 etc.

		return road_id

	def construct_token_infra_grammar(self):
		# *plein, *brug, *duct, *tunnel

		token_infrax = oneOf(["plein","brug","duct","tunnel","knooppunt"], caseless=True)

		#pattern_roadworks = Combine(SkipTo(oneOf(["werkzaamheden","onderhoud"], caseless=True), include=True))
		token_infrax1 =  Combine(Optional(Literal("#"))+Word(alphas, exact=1)+SkipTo(oneOf(["plein","brug","duct","tunnel"], caseless=True), include=True, failOn=White()))

        # Optional added by Ali, 2015/12/27
		return self.token_infra1 + Optional(token_infrax|token_infrax1)

	def construct_number_pattern1(self):
		return WordStart()+Word(nums, min=1, max=2) + WordEnd()

	def construct_number_pattern2(self):
		return WordStart()+Word(nums, min=2, max=3) + Optional(Literal(".") + Word(nums, exact=1))

	def construct_token_marker(self):
		return WordStart()+oneOf(["hectometerpaal","hmp","HMP","kilometeraanduiding","kilometerpaal","hectometerpaaltje","kilometerpaaltje"], caseless=True)+WordEnd()

	def construct_road_section_grammar(self):
		# define: *plein, *brug, *duct, *tunnel
		
		token_infra = self.construct_token_infra_grammar()

		token_marker = self.construct_token_marker()

		number_pattern1 = self.construct_number_pattern1()
		number_pattern2 = self.construct_number_pattern2()

		number_pattern = number_pattern1 | number_pattern2

		arrow_pattern = Literal("->>")|Literal("->")|Literal(">>")| Literal("◄►") | Literal("&gt;") | Literal(">")|Literal("-")|Literal("►")|Literal("➡️")

		# transfer tussen (het/de/.. infra) p1 en (het/de/.. infra) p2 to here!!!!

		# DO: first number_pattern2 can not be same as the second number_pattern2
		road_section1 = Group(self.tssnLit + CaselessLiteral("afrit") + number_pattern1 + self.enLit + \
		                Optional(~token_infra+Optional(self.sm_token)+token_infra + \
		                Optional(oneOf(["met de","op de", "naar de"], caseless=True))) + self.place_names)

		road_section1x = Group(self.tssnLit + Optional(~self.place_names+Optional(self.sm_token)+token_infra) + self.place_names + self.enLit + \
		                Optional(~self.place_names+Optional(self.sm_token)+token_infra) + self.place_names)

		road_section2 = Group((self.tssnLit + token_marker + number_pattern2 + self.enLit + Optional(token_marker) + number_pattern2))
		road_section3 = Group(self.vanLit + token_marker + number_pattern2 + self.totLit + Optional(token_marker) + number_pattern2)

		road_section3x = Group(self.vanLit + self.place_names + number_pattern2 + arrow_pattern + self.place_names + Optional(",").suppress())

		road_section3xx = Group(self.place_names + Literal('-') + self.place_names)

		road_section4 = Group(self.vanafLit + token_marker + number_pattern2 + self.totLit + Optional(~number_pattern2+token_marker) + number_pattern2)
		road_section5 = Group(self.vanLit + token_marker + number_pattern2 + self.totAanLit + Optional(~number_pattern2+token_marker) + number_pattern2)
		road_section6 = Group(self.vanafLit + token_marker + number_pattern2 + self.totAanLit + Optional(~number_pattern2+token_marker) + number_pattern2)

		# "de brug naar de tunnel Amsterdam"
		road_section8 = Group(self.vanafLit + \
		    Optional( Optional(~(self.place_names | token_infra)+self.sm_token) +\
		               Optional(~(self.place_names | token_infra)+self.sm_token)+ 
		            token_infra)+\
		    self.place_names + \
		    Optional( Optional(~token_infra+self.sm_token) +\
		               Optional(~token_infra+self.sm_token)+ 
		            token_infra))

		road_section7x = Group(token_marker + number_pattern2 + arrow_pattern + number_pattern2)

        # try to match at the end. Added by Ali, 2015/12/26
		road_section9 = Group(self.place_names + arrow_pattern + self.place_names) # + Optional(",").suppress()

		def road_section_trnsfrm(s, loc, toks):
			tdict = toks.asDict()["road_section"]
			return {'start':tdict["locn_start"],'end':tdict["locn_end"], 'type':'road_section', 'road_section':" ".join(tdict["value"]), 'tokens':s[tdict["locn_start"]:tdict["locn_end"]]}


		road_section = road_section1 | road_section2 | road_section3 | road_section4 | road_section5 | road_section6 | \
		    road_section7x | road_section1x | road_section3x | road_section3xx | road_section8 | road_section9


		road_section = locatedExpr(road_section)
		road_section = road_section.setResultsName("road_section")
		road_section.setParseAction(road_section_trnsfrm)

		return road_section


	def construct_lane_grammar(self):
		token_lane = WordStart() + oneOf(["baan","hoofdrijbaan","hoofd- en parallelrijbaan","linkerrijbaan","linkerrijstrook",
                    "parallelbaan","parallelrijbaan","parallelrijstrook","parallelstrook","rechterrijbaan",
                   "rechterrijstrook","rijbaan","spitsstrook","strook","voorsorteerstrook","vluchtstrook", "een rijstrook",
                    "linkerbaan","rechterbaan","linkerstrook","rechterstrook", "tegenovergestelde rijbaan","alle rijstroken"], caseless=True) + WordEnd()

		def lane_pttrn_trnsfrm(loc,toks):
			return {'start':loc,'type':'lane','lane':toks["lane"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		lane_pattern1 =  Optional((self.opLit | self.opzijvanLit | self.naastLit) + Optional(~token_lane+self.sm_token)) + token_lane.setResultsName("lane")
		lane_pattern1.setParseAction(lane_pttrn_trnsfrm)

		lane_pattern = lane_pattern1

		return lane_pattern

	def construct_road_side_grammar(self):
		def token_side_trnsfrm(loc, toks):
			return {'start':loc,'end':loc+len(" ".join(toks)), 'type':'road_side', 'road_side':" ".join(toks),"tokens":" ".join(toks)}

		token_side = WordStart() + oneOf(['links','li','rechts','re'], caseless=True)+WordEnd()
		token_side.setParseAction(token_side_trnsfrm)

		return token_side

	def construct_road_point_grammar(self):
		# order rules from complex to simple! Otherwise, longest match will not work!
		def road_point1_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["infra"][0], "road":toks["road"][0],"tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point2_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["infra"][0], 'place':toks["place"][0],"tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point3_trnsfrm(loc,toks):
			return {"start":loc, "tokens":toks,"end":loc+len(" ".join(toks))}
		#return {'start':loc,'type':'road_point','road_point':toks["road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point4_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point5_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point6_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point7_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		def road_point8_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_point','road_point':toks["road"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		road_pattern1 = self.construct_road_pattern1()
		road_pattern2 = self.construct_road_pattern2()
		token_infra = self.construct_token_infra_grammar()
		number_pattern1 = self.construct_number_pattern1()
		number_pattern2 = self.construct_number_pattern2()
		token_road = self.construct_token_road()
		token_marker = self.construct_token_marker()

		road_pattern = road_pattern1 | road_pattern2
		road_pattern2 = road_pattern | token_road
		# No match! Is it correct?
		road_point1 = Group((self.opzijvanLit|self.naastLit) + Optional(~token_infra+self.sm_token) + token_infra.setResultsName("infra") + \
		               self.naarLit + Optional(~road_pattern2+self.sm_token) + road_pattern2) #.setResultsName("road"))
		#road_point1.setParseAction(road_point1_trnsfrm)

		road_point2 = Group((self.opzijvanLit|self.naastLit) + Optional(~token_infra+self.sm_token) + token_infra.setResultsName("infra") + \
		               self.naastLit + self.place_names) #.setResultsName("place"))
		# road_point2.setParseAction(road_point2_trnsfrm)

		#road_point3 = (opzijvanLit|naastLit) + Group(~road_pattern2+self.sm_token) + road_pattern2.setResultsName("road")
		road_point3 = Group((self.opzijvanLit|self.naastLit) + ~road_pattern+self.sm_token + road_pattern) #.setResultsName("road")
		#road_point3.setParseAction(road_point3_trnsfrm)

		road_point4 = Group((self.nabijLit | self.bijLit | self.vlakLit | self.naastLit | self.opLit | self.inLit) + self.sm_token + Optional(~token_infra+self.sm_token)+token_infra + Optional(self.naarLit) + Optional(self.place_names))
		#road_point4.setParseAction(road_point4_trnsfrm)

		road_point9 = Group((self.opzijvanLit | self.naastLit) + self.sm_token + token_road)
		#road_point9.setParseAction(road_point9_trnsfrm)

		road_point5 = Group((self.thvLit | self.bijLit | self.vlakLit | self.naastLit) + Optional(Optional(~token_infra+self.sm_token)+token_infra) + self.place_names)
		#road_point5.setParseAction(road_point5_trnsfrm)

		road_point6 = Group((self.thvLit | self.bijLit | self.vlakLit | self.naastLit) + CaselessLiteral('afrit') + number_pattern1)
		#road_point6.setParseAction(road_point6_trnsfrm)

		road_point7 = Group((self.thvLit | self.bijLit | self.vlakLit | self.naastLit) + token_marker + number_pattern2)
		#road_point7.setParseAction(road_point7_trnsfrm)

		road_point1 = Group((self.opzijvanLit | self.naastLit) + Optional(~token_infra+self.sm_token) + token_infra + self.naarLit + Optional(self.sm_token) + \
		                token_road)
		    
		road_point2 = Group((self.opzijvanLit | self.naastLit) + Optional(~token_infra+self.sm_token) + token_infra + self.naarLit + self.place_names)

		road_point8 = Group((self.tennoordenvanLit|self.tenwestenvanLit|self.tenzuidenvanLit|self.tenoostenvanLit) + self.place_names)
		#road_point8.setParseAction(road_point8_trnsfrm)

		def road_point_trnsfrm(s, loc, toks):
			tdict = toks.asDict()["road_point"]
			return {'start':tdict["locn_start"],'end':tdict["locn_end"], 'type':'road_point','road_point':" ".join(tdict["value"]), 'tokens':s[tdict["locn_start"]:tdict["locn_end"]]}

		## checkfeedback: road_point1 | road_point2  ???

		road_point = road_point1 | road_point2 | road_point3 | road_point4 | road_point5 | road_point6 | road_point7 | road_point8 | road_point9
		
		road_point = locatedExpr(road_point)
		road_point = road_point.setResultsName("road_point")
		road_point.setParseAction(road_point_trnsfrm)

		return road_point

	def construct_road_condition_grammar(self):
		road_condit_tokens = WordStart()+oneOf(["spoorvorming","wegverzakking","verzakking"], caseless=True)+WordEnd()

		road_condit_adj = WordStart()+oneOf(["beschadigd","beschadigde","gesmolten","glad","hobbelig","nat",
                                     "opgebroken","smeltend","uitgehold","verzakt","hobbelige","natte","gladde","verzakte"], caseless=True)+WordEnd()

		road_like_token = WordStart()+oneOf(["asfalt","bestrating","wegdek"], caseless=True)+WordEnd()

		def road_condit_pttrn_trnsfrm(loc,toks):
			return {'start':loc,'type':'road_condit','road_condit':toks["road_condit"], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		road_condit_pattern1 = road_condit_adj + road_like_token
		road_condit_pattern2 = road_condit_tokens 

		road_condit_pattern = road_condit_pattern1 | road_condit_pattern2
		road_condit_pattern = road_condit_pattern.setResultsName("road_condit")
		road_condit_pattern.setParseAction(road_condit_pttrn_trnsfrm)

		road_cond_pattern =  Optional((self.vanwegeLit | self.doorLit)+Optional(self.sm_token)) + road_condit_pattern

		return road_cond_pattern

	def construct_direction_grammar(self):

		token_direction = WordStart()+oneOf(['rijrichting', 'richting','ri',"->>","->",'>>','->',"◄►","&gt;", '>',"►","➡️","ri.","richtingen","in de richting van"], caseless=True)+WordEnd()
		token_adj_direction = WordStart()+oneOf(['noordelijke','westelijke','oostelijke','zuidelijke','beide',\
		                                         'tegenovergestelde',"noordoostelijke","zuidwestelijke", "noordwestelijke","zuidoostelijke", "tegengestelde"], caseless=True)+WordEnd()
		token_province = WordStart()+oneOf(['groningen','friesland','drente','overijssel','gelderland','limburg','noord-brabant',\
		                                   'utrecht','noord-holland','zuid-holland','zeeland','flevoland'], caseless=True)+WordEnd()# for future!
		token_country = WordStart()+oneOf(['belgie','belgië', 'duitsland'], caseless=True)+WordEnd()
		token_direction2 = WordStart()+oneOf(['vanuit het zuiden','vanuit het noorden','vanuit het oosten','vanuit het westen',\
		                                     'noordwaarts','zuidwaarts','oostwaarts','westwaarts','noord-zuid','oost-west','naar het noorden','naar het zuiden', \
		                                     'naar het oosten', 'naar het westen','naar het noordoosten','naar het zuidwesten','naar het noordwesten','naar het zuidoosten'], caseless=True)+WordEnd()

		direction_pttrn1 = (self.inLit | self.vanuitLit) + Optional(~token_direction+self.sm_token) + token_direction + \
		                Optional(~(self.place_names | token_province | token_country) + self.sm_token) + (self.place_names | token_province | token_country)
		    
		direction_pttrn2 = WordStart() + self.inLit + token_adj_direction + CaselessLiteral('richting') + WordEnd()

		def direction_pttrn_trnsfrm(loc, toks):
		    #return {'start':loc,'end':loc+len(" ".join(toks)),'type':'direction','direction':toks["direction"][0],'tokens':" ".join(toks)}
			return {'start':loc,'end':loc+len(" ".join(toks)),'type':'direction','direction':" ".join(toks),'tokens':" ".join(toks)}
		
		#token_direction2 = token_direction2.setResultsName("direction")
		#direction_pttrn1 = direction_pttrn1.setResultsName("direction")
		#direction_pttrn2 = direction_pttrn2.setResultsName("direction")

		direction_pttrn3 = token_direction + (self.place_names|token_province|token_country)

		direction_pttrn4 = WordStart() + self.vanLit + self.place_names + self.naarLit + self.place_names + WordEnd()

		direction_pttrn5 = WordStart() + self.naarLit + self.place_names + WordEnd()

		direction_pttrn = direction_pttrn1 | direction_pttrn2 | token_direction2 | direction_pttrn3 | direction_pttrn4 | direction_pttrn5
		direction_pttrn.setParseAction(direction_pttrn_trnsfrm)

		return direction_pttrn

	def construct_location_grammar(self):
		# include river names, park areas!!
		token_infra = self.construct_token_infra_grammar()
		token_marker = self.construct_token_marker()
		number_pattern2 = self.construct_number_pattern2()

		location_pttrn1 = (self.inLit | self.bijLit  | self.vlakLit | self.naastLit | self.thvLit | self.voorLit | self.opLit) + \
		                    Optional(~token_infra+Optional(self.sm_token)+token_infra +~self.place_names + \
		                                            Optional(self.sm_token)) + self.place_names.setResultsName("place")

		location_pttrn2 = token_marker + number_pattern2
		location_pttrn3 = (self.inLit | self.bijLit  | self.vlakLit | self.naastLit | self.thvLit) + location_pttrn2.setResultsName("location")

		location_pttrn4 = self.place_names.setResultsName("place")

		def location_pttrn1_trnsfrm(loc,toks):
		    #return {'start':loc,'type':'location','location':toks["place"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}
		    return {'start':loc,'type':'location','location':" ".join(toks["place"]), "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		def location_pttrn3_trnsfrm(loc,toks):
		    # make better: 'location':"".join(toks["location"])
		    return {'start':loc,'type':'location','location':" ".join(toks["location"]), "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		location_pttrn1.setParseAction(location_pttrn1_trnsfrm)
		location_pttrn3.setParseAction(location_pttrn3_trnsfrm)
		location_pttrn4.setParseAction(location_pttrn1_trnsfrm)


		location_pttrn = location_pttrn1 | location_pttrn3 | location_pttrn4

		return location_pttrn

	def construct_status_grammar(self):
		status_token = WordStart()+oneOf(["afgesloten","afgezet","beeindigd","dicht","geblokkeerd","gestremd","nagenoeg opgelost",
                      "ontsloten","open","opengesteld","opgehaald","opgelost","versperd","vrij","vrijgemaakt",
                      "vrijwel opgelost","zo goed als opgelost","zo goed als weg", "vrijgegeven","op slot gezet"], caseless=True) + WordEnd(alphas)

		def status_pttrn_trnsfrm(loc,toks):
			return {'start':loc,'type':'status','status':toks["status"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		status_token = status_token.setResultsName("status")
		status_token.setParseAction(status_pttrn_trnsfrm)
		status_pttrn = status_token

		return status_pttrn

	def construct_traffic_grammar(self):
		
		token_traffic1 = WordStart()+oneOf(["bestemmingsverkeer","bijzonder transport","avondspits","colonne","containervervoer",\
                      "doorgaand verkeer","goederenvervoer","ochtendspits","pendelverkeer","stadsverkeer",\
                       "transport","vakantieverkeer","veetransport","vrachtverkeer","werkverkeer",\
                       "woon-werkverkeer","spitsverkeer","spits","paasdrukte","pinksterdrukte","vakantiedrukte",\
                      "verkeer","vervoer"], caseless=True)+WordEnd()

		# define: *vervoer, *verkeer, *transport, *drukte
		pattern_traffic = Combine(Optional(Literal("#"))+Word(alphas, exact=1)+SkipTo(oneOf(["vervoer","verkeer","transport","drukte"], caseless=True), include=True, failOn=White())) # Optional('#')+Word(alphas)+

		token_traffic = token_traffic1 | pattern_traffic

		def traffic_pttrn_trnsfrm(loc,toks):
			return {'start':loc,'type':'traffic','traffic':toks["traffic"], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		token_traffic = token_traffic.setResultsName("traffic")
		token_traffic.setParseAction(traffic_pttrn_trnsfrm)
		traffic_pttrn = token_traffic

		return traffic_pttrn

	def construct_flow_grammar(self):
		# add third person verb form
		flow_tokens = WordStart()+oneOf(["stilstaan","langzaam rijden","rijden","staan","stagneren","stokken","vastlopen","versnellen",\
		                     "vertragen","rijdend","stilstaand","langzaam rijdend","stagnerend","stokkend","vastlopend",\
		                     "versnellend","vertragend","staat stil","rijdt langzaam", "stapvoets rijdend","rijdt stapvoets","versnelt","rijdt","stagneert",\
		                     "stokt","loopt vast","vertraagt","stond stil","stil gestaan","reed langzaam","langzaam gereden",\
		                     "stagneerde","gestagneerd","vertraagde","vertraagd","liep vast","vastgelopen","stil gestaan"], caseless=True)+WordEnd() #

		def flow_pttrn_trnsfrm(loc,toks):
			return {'start':loc,'type':'flow','flow':toks["flow"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		flow_tokens = flow_tokens.setResultsName("flow")
		flow_tokens.setParseAction(flow_pttrn_trnsfrm)
		flow_pttrn = flow_tokens

		return flow_pttrn

	def construct_intensity_grammar(self):
		intensity_tokens = WordStart()+oneOf(["drukte","rust","verkeersdichtheid","verkeersdrukte","verkeersintensiteit"], caseless=True)+WordEnd()
		intensity_tokens = intensity_tokens.setResultsName("intensity_token")

		intensity_adj_intens = WordStart()+oneOf(["druk","rustig"], caseless=True)+WordEnd()
		intensity_adj_intens = intensity_adj_intens.setResultsName("intensity_adj_intens")

		intensity_adj_mod = WordStart()+oneOf(["bijzondere","enorme","erge","gematigde","grote","uitzonderlijke"], caseless=True)+WordEnd()
		intensity_adj_mod = intensity_adj_mod.setResultsName("intensity_adj_mod")

		intensity_adv = WordStart()+oneOf(["bijzonder","enorme","erg","gematigd","matig","nogal","tamelijk","uiterst","uitzonderlijk",\
		                       "vrij","zeer"], caseless=True)+WordEnd()
		intensity_adv = intensity_adv.setResultsName("intensity_adv")

		# order should be correct!
		# WordBegin and Word End!
		def intensity_pattern1_trnsfrm(loc, toks):
			return {'start':loc,'type':'intensity_pattern1','intensity_pattern1':toks["intensity_pattern1"]["intensity_adj_intens"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		def intensity_pattern2_trnsfrm(loc, toks):
			return {'start':loc,'type':'intensity_pattern2','intensity_pattern2':toks["intensity_pattern2"]["intensity_token"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}


		intensity_pattern1 = Optional(~intensity_adj_intens + intensity_adv) + intensity_adj_intens
		intensity_pattern1 = intensity_pattern1.setResultsName("intensity_pattern1")
		intensity_pattern1.setParseAction(intensity_pattern1_trnsfrm)

		intensity_pattern2 = Optional(~intensity_tokens + intensity_adj_mod) + intensity_tokens
		intensity_pattern2 = intensity_pattern2.setResultsName("intensity_pattern2")
		intensity_pattern2.setParseAction(intensity_pattern2_trnsfrm)

		intensity_pttrn = intensity_pattern1 | intensity_pattern2

		return intensity_pttrn

	def construct_speed_grammar(self):
		speed_adj = WordStart()+oneOf(["gematigde","hoge","maximum","minimum"], caseless=True)+WordEnd()
		# *snel, *traag, *hard, *langzam # just the beginning of a word
		speed_adv = WordStart()+oneOf(["hard","langzaam","snel","stapvoets","scheurend","te hard","traag","harder","langzamer","trager","sneller"], caseless=True)+WordEnd()
		# *snelheid
		speed_noun = WordStart()+oneOf(["adviessnelheid","kruissnelheid","snelheid","snelheidsbeperking","snelheidslimiet",
		                    "topsnelheid"], caseless=True)+WordEnd()

		def speed_trnsfrm(loc,toks):
			return {'start':loc,'type':'speed','speed':toks["speed"], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		speed_pttrn = speed_adj | speed_adv | speed_noun
		speed_pttrn = speed_pttrn.setResultsName("speed")
		speed_pttrn.setParseAction(speed_trnsfrm)
		
		return speed_pttrn

	def construct_weather_grammar(self):

		token_weather = WordStart()+oneOf(["bewolking","dooi","duisternis","mist","nattigheid","nevel","onweer","regen","regenval",
                      "sneeuwstorm","sneeuw","storm","wind","windstoten","windvlagen","weersomstandigheden",
                       "zicht","zon","hagel","ijzel","ijs","ijsvorming"], caseless=True)+WordEnd()

		token_weather2 =  WordStart()+oneOf(["weer"], caseless=True)+WordEnd()


		token_weather = token_weather.setResultsName("token_weather")
		token_weather2 = token_weather2.setResultsName("token_weather")

		token_weather_adj = WordStart()+oneOf(["beginnende","beperkt","dichte","heftige","invallende","krachtige","laagstaande",
		                          "laaghangende","matige","matig","uitzonderlijke","verraderlijke","zware", "heerlijk", "slecht", 
		                          "zalig", "fijn", "belabberd", "waardeloos", "geweldig", "fantastisch", "vreselijk"], caseless=True)+WordEnd()
		token_weather_adj = token_weather_adj.setResultsName("token_weather_adj")


		weather_condition = (self.vanwegeLit | self.metLit | self.doorLit | self.tijdensLit) + Optional((~(token_weather | token_weather_adj)+OneOrMore(self.sm_token))) +\
		                    Optional(token_weather_adj) + token_weather

		#weather_condition2 = (self.vanwegeLit | self.metLit | self.doorLit | self.tijdensLit) + Optional((~(token_weather | token_weather_adj)+OneOrMore(self.sm_token))) + token_weather_adj + token_weather2
		weather_condition2 = Optional((self.vanwegeLit | self.metLit | self.doorLit | self.tijdensLit) + Optional((~(token_weather | token_weather_adj)+OneOrMore(self.sm_token)))) + token_weather_adj + token_weather2
		#weather_condition2 = Optional((self.vanwegeLit | self.metLit | self.doorLit | self.tijdensLit)) + Optional((~(token_weather | token_weather_adj)+OneOrMore(self.sm_token))) + token_weather_adj + token_weather2
		#weather_condition2 = token_weather_adj + token_weather2

		def weather_cond_trnsfrm(loc, toks):
			return {'start':loc,'type':'weather_cond','weather_cond':toks["weather_cond"]["token_weather"][0], "tokens":" ".join(toks), 'end':loc+len(" ".join(toks))}

		weather_condition = weather_condition.setResultsName("weather_cond")
		weather_condition2 = weather_condition2.setResultsName("weather_cond")
		weather_condition.setParseAction(weather_cond_trnsfrm)
		weather_condition2.setParseAction(weather_cond_trnsfrm)
		weather_cond_pttrn = weather_condition2 | weather_condition

		return weather_cond_pttrn


	def construct_traffic_violations_grammar(self):
		traffic_vio_token = WordStart()+oneOf(["bumperkleven","snijden","spookrijden"],caseless=True) + WordEnd()

		def traffic_vio_trnsfrm(loc, toks):
			return {'start':loc,'type':'traffic_vio','traffic_vio':toks["traffic_vio"][0], "tokens":toks[0], 'end':loc+len(" ".join(toks))}

		traffic_vio_token = traffic_vio_token.setResultsName("traffic_vio")
		traffic_vio_token.setParseAction(traffic_vio_trnsfrm)

		return traffic_vio_token

	def construct_monitoring_grammar(self):
		# add *controle variations
		monitoring_token =  WordStart()+oneOf(["alcoholcontrole","controle","flitser","radarcontrole","signalering","snelheidscontrole",\
                         "trajectscontrole","verkeerscontrole","alcoholcontroles","controles","flitsers","radarcontroles","signaleringen","snelheidscontroles",\
                         "trajectscontroles","verkeerscontroles"], caseless=True)+ WordEnd()

		def monitoring_trnsfrm(loc, toks):
			return {'start':loc,'type':'monitoring','monitoring':toks["monitoring"][0], "tokens":toks[0], 'end':loc+len(" ".join(toks))}

		monitoring_token = monitoring_token.setResultsName("monitoring")
		monitoring_token.setParseAction(monitoring_trnsfrm)

		return monitoring_token

	def construct_development_grammar(self):
		# should we add type of development?
		development_token =  WordStart()+oneOf(["lost langzaam op","lost snel op","neemt af","neemt langzaam af","neemt snel af",\
		                                       "neemt toe","wordt korter","wordt langzaam korter","wordt snel korter","wordt langer",\
		                                       "is opgelost","is bijna opgelost","is vrijwel opgelost","wordt opgeheven","wordt beeindigd",\
		                                       "is opgeheven","is beeindigd"], caseless=True)+ WordEnd()
		def development_trnsfrm(loc, toks):
			return {'start':loc,'type':'development','development':toks["development"][0], "tokens":toks[0], 'end':loc+len(" ".join(toks))}

		development_token = development_token.setResultsName("development")
		development_token.setParseAction(development_trnsfrm)
		return development_token

	def construct_activity_grammar(self):
		activity_token = WordStart() + oneOf(["omrijden", "uitwijken", "uitwijkt"],caseless=True) + WordEnd()
		# consider: uitwijkroute, uitwijkroute, omleidingsroute
		def activity_trnsfrm(loc, toks):
		    return {'start':loc,'type':'activity','activity':toks["activity"][0], "tokens":toks[0], 'end':loc+len(" ".join(toks))}

		activity_token = activity_token.setResultsName("activity")
		activity_token.setParseAction(activity_trnsfrm)
		return activity_token

	def construct_traffic_event_grammar2(self):
		notify_token = WordStart() + oneOf(["afsluiting","afzetting","chaos","file","filevorming","verkeershinder","hinder","omleiding",\
                            "oponthoud","opstopping","ramp","spookrijder","spookrijdster","storing","verkeerschaos","verkeershinder",\
                            "verkeersinfarct","verkeersopstopping","versperring","wachttijd","wegafzetting",\
                                    "wegversperring","vertraging","verkeersstremming"],caseless=True) + WordEnd()

		def notify_trnsfrm(loc, toks):
			return {'start':loc,'type':'alert','alert':toks["alert"][0], "tokens":toks[0], 'end':loc+len(" ".join(toks))}

		notify_token = notify_token.setResultsName("alert")
		notify_token.setParseAction(notify_trnsfrm)
		return notify_token

	def construct_advice_grammar(self):
		# consider to add new type tense of the verbs
		advice_token = WordStart() + oneOf(["aanraden","advies","adviseren","informatie","informeren","wordt aangeraden",\
                                    "wordt geadviseerd","opgelet","pas op","volg"],caseless=True) + WordEnd()

		def advice_trnsfrm(loc, toks):
			return {'start':loc,'type':'advice','advice':toks["advice"][0], "tokens":toks[0], 'end':loc+len(" ".join(toks))}

		advice_token = advice_token.setResultsName("advice")
		advice_token.setParseAction(advice_trnsfrm)
		return advice_token

	def construct_spatio_grammar(self):
		"""
           "road_id", "road_point", "road_section","road_side","direction","lane"
           NO: location! -> location is too ambiguous
		"""
		return self.construct_road_id_grammar() | self.construct_lane_grammar() | self.construct_road_section_grammar() \
				| self.construct_road_side_grammar() | self.construct_road_point_grammar() | self.construct_direction_grammar()

	def construct_traffic_info_grammar(self):
		"""
			"activity","advice","alert","development","flow","intensity_pattern1", "intensity_pattern2", "monitoring","road_condit","speed","status",
			"traffic","traffic_vio","weather_condit"

		NO: timex : too frequent, and ambiguous.

		"""
		return self.construct_traffic_event_grammar() | self.construct_status_grammar() | self.construct_traffic_grammar() | self.construct_flow_grammar() \
				| self.construct_intensity_grammar() | self.construct_speed_grammar() | self.construct_weather_grammar() | self.construct_traffic_violations_grammar() \
				| self.construct_monitoring_grammar() | self.construct_development_grammar() | self.construct_activity_grammar() | self.construct_traffic_event_grammar2() \
				| self.construct_advice_grammar()


	def construct_complete_traffic_grammar(self):
		traffic_grammar = self.construct_traffic_event_grammar() | self.construct_road_id_grammar() | self.construct_lane_grammar() | self.construct_road_section_grammar() \
				| self.construct_road_side_grammar() | self.construct_road_point_grammar() | self.construct_road_condition_grammar() | self.construct_direction_grammar() \
				| self.construct_location_grammar() | self.construct_status_grammar() | self.construct_traffic_grammar() | self.construct_flow_grammar() \
				| self.construct_intensity_grammar() | self.construct_speed_grammar() | self.construct_weather_grammar() | self.construct_traffic_violations_grammar() \
				| self.construct_monitoring_grammar() | self.construct_development_grammar() | self.construct_activity_grammar() | self.construct_traffic_event_grammar2() \
				| self.construct_advice_grammar()

		return self.construct_regex_based_timex_grammar() | traffic_grammar


	def extract_traffic_inf(self, textseries, output_option='css'):
		
		# traffic_grammar = self.construct_traffic_event_grammar() | self.construct_road_id_grammar() | self.construct_lane_grammar() | self.construct_road_section_grammar() \
		# 		| self.construct_road_side_grammar() | self.construct_road_point_grammar() | self.construct_road_condition_grammar() | self.construct_direction_grammar() \
		# 		| self.construct_location_grammar() | self.construct_status_grammar() | self.construct_traffic_grammar() | self.construct_flow_grammar() \
		# 		| self.construct_intensity_grammar() | self.construct_speed_grammar() | self.construct_weather_grammar() | self.construct_traffic_violations_grammar() \
		# 		| self.construct_monitoring_grammar() | self.construct_development_grammar() | self.construct_activity_grammar() | self.construct_traffic_event_grammar2() \
		# 		| self.construct_advice_grammar()

		# whole_grammar = self.construct_regex_based_timex_grammar() | traffic_grammar
		whole_grammar = self.construct_complete_traffic_grammar()

		search_result = [(tw, whole_grammar.searchString(tw)) for tw in textseries]

		#with open("place_names.txt")

		return self.merge_identified_inf(search_result, output_option)

	def dict_to_ordered_keys_str(self, mydict):
		"""
		      This is for keeping the order of the keys consistent. It is important to keep track of the changes.
		"""
    
		dict_to_key_ordered_str = '{'
		len_dict = len(mydict)

		for i,k in enumerate(sorted(mydict.keys())):

		    dict_to_key_ordered_str += "\"" + k + "\""+':' + "\"" + str(mydict[k]) + "\""
		    if i < (len_dict-1):
		        dict_to_key_ordered_str += ', '

		dict_to_key_ordered_str += '}'
		return dict_to_key_ordered_str

	# def get_type_based_tag_annotation(self, mydict):
	# 	return "<"+mydict["type"]+">"


	def merge_identified_inf(self, search_result, output_option='css'):

		tagged_tweet_list = []

		for tw_match in [sr[0] if len(sr[1])==0 else (sr[0],sr[1]) for sr in search_result]:

			if isinstance(tw_match, str):
				tagged_tweet_list.append(tw_match) # no match here, get just the tweet.
				continue
			elif isinstance(tw_match, tuple):
				tw, match = tw_match

			all_match_info = "TWEET:"+tw

			new_tw = tw[:match[0][0]["start"]] # beginning until first match.
			new_tw2 = tw[:match[0][0]["start"]] # short version
			match_list = []
			for i, m in enumerate(match):
				m = m[0] # get the dictionary
				match_list.append(m)

				all_match_info += "\nMATCH:"+str(i)+" "+self.dict_to_ordered_keys_str(m)

				try:
					new_tw += "<annotation " + self.dict_to_ordered_keys_str(m)[1:-1]+"> "+m["tokens"]+" </annotation>"
					new_tw2 += "<" + m["type"] + ">" + m["tokens"] + "<span>" + m["type"] + " </span></" + m["type"] + ">"
				except:
					new_tw += "<annotation "+ self.dict_to_ordered_keys_str(m)[1:-1]+"> "+m["tokens"]+" </annotation>"
					new_tw2 += "<" + m["type"] + ">" + m["tokens"] + "<span>" + m["type"] + " </span></" + m["type"] + ">"

				if i == 0 and len(match)>1:
					new_tw += tw[m["end"]:match[i+1][0]["start"]] # until the next match
					new_tw2 += tw[m["end"]:match[i+1][0]["start"]] # until the next match
				elif (i > 0) and (i < len(match)-1):
					new_tw += tw[m["end"]:match[i+1][0]["start"]] # until the next match
					new_tw2 += tw[m["end"]:match[i+1][0]["start"]] # until the next match

				elif (len(match) == 1) or (i == len(match)-1):
					new_tw += tw[m["end"]:] # until the end
					new_tw2 += tw[m["end"]:] # until the end
	
			if output_option == 'annotation':
				all_match_info+="\nAnnotated TWEET:"+new_tw #+"\nSHORT Annotated TWEET:" #+new_tw2
			elif output_option == 'css':
				all_match_info+="\nAnnotated TWEET:"+new_tw2 #+"\nSHORT Annotated TWEET:" #+new_tw2
			else:
				raise ValueError('This output_option is not available', output_option)
			tagged_tweet_list.append((all_match_info+"\n",match_list))


		return tagged_tweet_list
		



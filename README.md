## Motivation

SInfEx developed to extract information from social media. It contains mainly grammars to detect traffic domain in Dutch.

## Contributors

Ali Hürriyetoglu, Nelleke Oostdijk, Marco Puts, Piet Daas, Antal van den Bosch, Mustafa Erkan Başar

## Publications

Oostdijk N., Hürriyetoglu A., Puts M., Daas P., van den Bosch A. P. J. (2016). Information extraction from social media: A linguistically motivated approach. In proceedings of the Workshop on Risk and NLP, July 4, 2016, Paris, France.

Presentation: http://www.slideshare.net/AliHrriyetolu/information-extraction-from-social-media-a-linguistically-motivated-approach

## Demo

http://sinfex.science.ru.nl

## License

Licensed under GPLv3 (See http://www.gnu.org/licenses/gpl-3.0.html)

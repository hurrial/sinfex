import sys
sys.path.insert(1, r'../datautils/')  # add to pythonpath"

import sinfex as sie # social information extraction
from datautils import fileutils
import pandas as pd
import difflib

input_file = "baseline/baseline_tweets.txt" # it is tokenized tweets per line
output_file = "baseline/recent_sinfex_traffic_result.txt"
baseline_file = "baseline/sinfex_traffic_baseline.txt"

test_tweets_series = pd.Series(fileutils.getlinelist(input_file))

extractor = sie.Sinfex()

traffic_result_tweets = extractor.extract_traffic_inf(test_tweets_series, output_option='annotation') 

traffic_result_tweets_str = [t if isinstance(t,str) else t[0] for t in traffic_result_tweets]
fileutils.writelinelist(output_file, traffic_result_tweets_str)


file1 = open(output_file, 'r')
file2 = open(baseline_file, 'r')

diff = difflib.ndiff(file1.readlines(), file2.readlines())
print("The difference is:")
print(''.join([l for l in diff if l.startswith('+ ') or l.startswith('- ')]))

import sys

import getpass

if getpass.getuser() == "a.hurriyetoglu":
    sys.path.insert(1, r'../datautils/')  # add to pythonpath
else:
    sys.path.insert(1, r'/home/hurrial/datautils/')  # add to pythonpath

import configparser
import tweepy
from tweepy.streaming import StreamListener
import json

import sinfex as sie # social information extraction
from datautils import fileutils
import statistics as s
import pandas as pd
import importlib
import json

from pymongo import MongoClient
import smtplib
from datetime import datetime
import logging

datafolder = ""
if getpass.getuser() == "a.hurriyetoglu":
    datafolder = "data/"  # add to pythonpath
else:
    datafolder = "/home/hurrial/sinfex/data/"  # add to pythonpath

logging.basicConfig(
		format='%(asctime)s, %(levelname)s: %(message)s',
		filename=datafolder+'sinfex.log',
		datefmt='%d-%m-%Y, %H:%M',
		level=logging.INFO)

configmongo = configparser.ConfigParser()
configmongo.read(datafolder+"localdb.ini")

db_host = configmongo.get('mongodb', 'client_host')
db_port = int(configmongo.get('mongodb', 'client_port'))
dbname = configmongo.get('mongodb', 'dbname')
collname = configmongo.get('mongodb', 'collname')    

config = configparser.ConfigParser()
config.read(datafolder+'oauth.ini')
config.sections()
for k in config['twitterapp']: print(k)
print("Twitter authentication file parsed")


def send_mail(sbj, msg, to):
	#Mail Auth;
	fromaddr = config.get('mail', 'fromaddr')
	toaddrs  = config.get('mail', to) # opt : hurrial, ebasar
	username = config.get('mail', 'user_name') 
	password = config.get('mail', 'password')
	subject = sbj
	message = 'Subject: ' + subject + '\n\n' + msg
	server = smtplib.SMTP('smtp.gmail.com:587')  
	server.starttls()  
	server.login(username,password)  
	server.sendmail(fromaddr, toaddrs, message)  
	server.quit()  


try:
    client = MongoClient(db_host, db_port)
    localdb = client[dbname]
    localcol = localdb[collname]
    print("MongoDB connected (database : " + dbname + ", collection : " + collname + ")")
except:
    print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
    logging.warn('MongoDB Connection Failed')
    send_mail("Sinfex - MongoDB", "MongoDB Connection Failed", "ebasar")


# use_wlistT = fileutils.getlinelist('timexall9.txt')

# mean_tmx_len = s.mean([len(t) for t in use_wlistT])
# use_wlistT_short = [t for t in use_wlistT if len(t)<mean_tmx_len] # use just the short ones.

# ignore_tmx = ['weer']

# print("Length of the tmx to be used:", len(use_wlistT_short))
# use_wlistT_short = [t for t in use_wlistT_short if t not in ignore_tmx]
# print("Length of the tmx to be used (after elimination of ignore_tmx):", len(use_wlistT_short))



importlib.reload(sie)
extractor = sie.Sinfex()
#extractor.set_timex_list(use_wlistT_short)

print("'extractor' initialized")


class MyStreamListener(StreamListener):

    def __init__(self):
        StreamListener.__init__(self)
        self.mynotified = False

        self.elim_users = ['flitsers','TenukiSoftware','ANWBeuropa','FlitsserviceMob','FlitsserviceNL','FlitsserviceUT',\
        'FlitsserviceLI','OW2377','Radarmelding','FlitsNL','Sparta_A2','FlitsNLA2','FlitsserviceGE','VeiligheidWeg',\
        'FlitsserviceNB','OpenbaarBestuur','meldeenflitser','FlitsserviceNH','FlitsersInfo','verkeereuropa',\
        'FlitsgoeroeRoy','TouringEuropa','Flitsnieuws_Be','Flitskast','Flitsservice_NL','FlitscontroleNL',\
        'FlitsGoeroeRoy_','FlitsNav','Flitser22',"tcapping2","t_mobilis_nl", "Ambulancepost","cop_spotter",\
        "NL_P2000","P2000_Regio18","p2000monitor","gld112","p2k_waddinxveen","112zw_info","MWBP2000","P2000WWB",\
        "controlesBE","P2000Regio6","FilenieuwsBE","AmbulanceUTR","P2000vanheelNL","FlitsserviceWVL","P2000Lochem",\
        "P2000Regio15","P2000_Roermonds", "p2kwestbrabant","p2000vanheelNL1","Vakantie_idee","AMBUGLD","jorisvpden",\
        "p2kutrecht", "p2khaaglanden","p2kkennemerland","p2kgooi","pbameldingen","DeetosSnelLive","Arrowescape",\
        "Doorntje071","112drenthe","BdrechtVliegen","Forza100","JSBTOMTOM","KCCLiveScores","FlaggNL","BrandweerMON",\
        "LucielRivera","Rein64","andrebroos1","112HM","Flynthtweets","DOS46live","kvhoogkerk","P2000Melding","112puttershoek",\
        "berryjans","dsehovic88","MeldingenNL","daveydaisy12","Flitslimburg","scHveenjeugd","AviRaymond","bartjaap1",\
        "112rijnmond","Leiderdorp112","FlitsserviceBEL","DOS46","SmeetsNorbert","FlitsserviceANT","12REG","alvanweverwijk",\
        "112assen","Qc511_Mtl","112petrus","DeStemVanGod","retweet0313","FlitsserviceLIE","P2000Regio23","1968joop","Arriva_NL",\
        "112zuidplas","tomfann17470431","kv_apollo","RTBFMobilinfo","planeetKim","ingehaald","FrankRuitenburg","WIS_Rene",\
        "112Vandaag","a_bulsara","Sorge110","MikeAviation","vv_hoogezand","FlitsserviceOVL","BRW_OGT","SamsungMobileNL",\
        "KOSC_Ootmarsum","weesp112","FlitsserviceLIM","p2kgooi","svRKDEO","FCGOnline","WSM_Schiedam","Das_Aut0","volgonzeTomtom",\
        "FlitsserviceBXL","P2000NN"]

        self.elim_users = [u.lower() for u in self.elim_users]

    def del_keys_with_default(self, node):
        """
        Recursively eliminate keys of a dictionary if a key has default value (tweet in our context)
        """
        if isinstance(node, list) or isinstance(node, str):
            if len(node) > 0:
                return node
            return None
        elif isinstance(node, int):
            if node > 0:
                return node
            return None
        elif isinstance(node, type(None)):
            return None
        else:
            dupe_node = {}
            for key, val in node.items():
                cur_node = self.del_keys_with_default(val)
                if cur_node:
                    dupe_node[key] = cur_node
            return dupe_node or None


    def not_Ambu(self, tw):
        tw = tw.lower()
        tok_set = set(tw.split())
        tok_set = set([t.strip(".:()!#[]?") for t in tok_set])
        if set(["a2","a1"]) & tok_set: # if the intersection is empty, it will be evaluated to False.
            if set(["ambu", "p2000","rit", "inzet", "ambulance"]) & tok_set: # if the intersection is empty, it will be evaluated to False.
                return False # if A1 or A2 exists, none of the elements from second list should occur.
        elif (set(["meldkamer"]) & tok_set) or ("ambulance naar" in tw):
            return False
        return True

    def on_status(self, status):
        tweetdict = {k:v for k,v in status._json.items()}

        now = (datetime.now()).strftime("%H:%M")

        if not self.mynotified and (now in ["06:15","14:15","22:15"]):# and flagsix == False):
            send_mail("Sinfex - Stream is running","Everything is up and running\n\n Time : " + now, "ebasar")
            send_mail("Sinfex - Stream is running","Everything is up and running\n\n Time : " + now, "hurrial")
            self.mynotified = True # Notify just once.

        if self.mynotified and (now not in ["06:15","14:15","22:15"]):
            self.mynotified = False # make it false, only if you are not at the moments that you should notify by email.

        
        if (tweetdict["lang"] == 'nl') and (tweetdict["user"]["screen_name"].lower() not in self.elim_users) and (self.not_Ambu(tweetdict["text"])): # it checks every condition until it come across a False.
            print(tweetdict["id_str"], tweetdict["text"])
#            print("https://twitter.com/"+ tweetdict['user']['screen_name'] +"/status/"+tweetdict['id_str'])

            tweetdict2 = self.del_keys_with_default(tweetdict)

            sinfex_rslt = extractor.extract_traffic_inf([tweetdict2["text"]])[0]

            tweetdict2["created_at"] = datetime.strptime(tweetdict2['created_at'],'%a %b %d %H:%M:%S +0000 %Y')


            if isinstance(sinfex_rslt, str): # this means there is not any match.
                #print("str sinfex result:",sinfex_rslt)
                localcol.insert(tweetdict2)
            else:        
                tweetdict2["sinfex_inf_str"], tweetdict2["sinfex_inf_dicts"] = sinfex_rslt 
                try:
                   localcol.insert(tweetdict2)
                except Exception as ex:
                   logging.error("Tweet Error for the tweet : " + tweetdict2["text"])
                   logging.error("Tweet Error Traceback : ", exc_info=True)
                   msg = "Tweet Error for the tweet :  https://twitter.com/" + tweetdict2['user']['screen_name'] + "/status/" + tweetdict2['id_str'] + "\n\nError message : " + str(ex) + "\n\nSee the log file for more information about the error\n\nDate : " + (datetime.now()).strftime("%H:%M %d-%m-%y") 
                   send_mail("Sinfex - Tweet Error", msg, "ebasar")
                   send_mail("Sinfex - Tweet Error", msg, "hurrial")
        
    def on_error(self, status_code):
        if status_code == 420:
            #returning False in on_data disconnects the stream
            print("Error 420")
            logging.error('Stream Error : Error 420', exc_info=True)
            send_mail("Sinfex - Stream Error", "Error 420", "ebasar")
            send_mail("Sinfex - Stream Error", "Error 420", "hurrial")
        else:
            print("Error code: ", status_code)
            logging.error('Stream Error : Error ' + str(status_code), exc_info=True)
            msg = "Error code: "+str(status_code)
            send_mail("Sinfex - Stream Error", msg, "ebasar")
            send_mail("Sinfex - Stream Error", msg, "hurrial")
           
        return False

         
myauth = tweepy.OAuthHandler(config['twitterapp']['consumer_key'], config['twitterapp']['consumer_secret'])
myauth.set_access_token(config['twitterapp']['access_token'], config['twitterapp']['access_token_secret'])

api = tweepy.API(myauth)

print("Twitter authentication done.")

print("Streaming started")


try:
    myStreamListener = MyStreamListener()
    myStream = tweepy.streaming.Stream(auth = myauth, listener=myStreamListener)
    myStream.filter(locations=[3.01,51.52,7.14,53.56])
except Exception as ex:
    logging.error('Stream Failed : ', exc_info=True)
    msg = "Stream Failed with the exception : " + str(ex) + "\n\nSee the log file for more information about the error\n\nDate : " + (datetime.now()).strftime("%H:%M %d-%m-%y") 
    send_mail("Sinfex - Stream Failed", msg, "ebasar")
    send_mail("Sinfex - Stream Failed", msg, "hurrial")

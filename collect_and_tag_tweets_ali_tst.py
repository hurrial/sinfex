import sys
sys.path.insert(1, r'../datautils/')  # add to pythonpath

import configparser
import tweepy
from tweepy.streaming import StreamListener
import json

import sinfex as sie # social information extraction
from datautils import fileutils
import statistics as s
import pandas as pd
import importlib
import json

from pymongo import MongoClient
import smtplib
from datetime import datetime
import logging

logging.basicConfig(
		format='%(asctime)s, %(levelname)s: %(message)s',
		filename='data/sinfex.log',
		datefmt='%d-%m-%Y, %H:%M',
		level=logging.INFO)

# configmongo = configparser.ConfigParser()
# configmongo.read("data/localdb.ini")

#db_host = configmongo.get('mongodb', 'client_host')
#db_port = int(configmongo.get('mongodb', 'client_port'))
#dbname = configmongo.get('mongodb', 'dbname')
#collname = configmongo.get('mongodb', 'collname')    

config = configparser.ConfigParser()
config.read('data/oauth.ini')
config.sections()
for k in config['twitterapp']: print(k)
print("Twitter authentication file parsed")


# def send_mail(sbj, msg, to):
# 	#Mail Auth;
# 	fromaddr = config.get('mail', 'fromaddr')
# 	toaddrs  = config.get('mail', to) # opt : hurrial, ebasar
# 	username = config.get('mail', 'user_name') 
# 	password = config.get('mail', 'password')
# 	subject = sbj
# 	message = 'Subject: ' + subject + '\n\n' + msg
# 	server = smtplib.SMTP('smtp.gmail.com:587')  
# 	server.starttls()  
# 	server.login(username,password)  
# 	server.sendmail(fromaddr, toaddrs, message)  
# 	server.quit()  


# try:
#     client = MongoClient(db_host, db_port)
#     localdb = client[dbname]
#     localcol = localdb[collname]
#     print("MongoDB connected (database : " + dbname + ", collection : " + collname + ")")
# except:
#     print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
#     logging.warn('MongoDB Connection Failed')
#     send_mail("Sinfex - MongoDB", "MongoDB Connection Failed", "ebasar")


use_wlistT = fileutils.getlinelist('timexall9.txt')

mean_tmx_len = s.mean([len(t) for t in use_wlistT])
use_wlistT_short = [t for t in use_wlistT if len(t)<mean_tmx_len] # use just the short ones.


importlib.reload(sie)
extractor = sie.Sinfex()
extractor.set_timex_list(use_wlistT_short)
notified = False
print("at start notified:", notified)

def heart_beat():

    now = (datetime.now()).strftime("%H:%M")

    if (not notified) and (now in ["06:15","14:15","22:15"]):
        print("Sinfex - Stream","Everything is up and running\n\n Time : " + now, "ebasar")
        notified = True # Notify just once.

    if notified and (now in ["06:15","14:15","22:15"]):
        notified = False # make it false, only if you are not at the moments that you should notify by email.


print("'extractor' initialized")


class MyStreamListener(StreamListener):

    def __init__(self):
        StreamListener.__init__(self)
        self.mynotified = False

    # def set_notification(self, mynotified=False):
    #     self.mynotified = mynotified

    def del_keys_with_default(self, node):
        """
        Recursively eliminate keys of a dictionary if a key has default value (tweet in our context)
        """
        if isinstance(node, list) or isinstance(node, str):
            if len(node) > 0:
                return node
            return None
        elif isinstance(node, int):
            if node > 0:
                return node
            return None
        elif isinstance(node, type(None)):
            return None
        else:
            dupe_node = {}
            for key, val in node.items():
                cur_node = self.del_keys_with_default(val)
                if cur_node:
                    dupe_node[key] = cur_node
            return dupe_node or None

    def on_status(self, status):
        tweetdict = {k:v for k,v in status._json.items()}

        now = (datetime.now()).strftime("%H:%M")
        print("Now is:", now)
        print("self.mynotified is:", self.mynotified)

       # heart_beat()
        if not self.mynotified and (now in ["06:15","14:15","22:15","21:53"]):# and flagsix == False):
            print("\n\n **** Sinfex - Stream","Everything is up and running\n\n Time : " + now, "ebasar ****\n\n")
            self.mynotified = True # Notify just once.

        if self.mynotified and (now not in ["06:15","14:15","22:15","21:53"]):
            print("\n\n **** Notification is closed ****\n\n")
            self.mynotified = False # make it false, only if you are not at the moments that you should notify by email.

        if tweetdict["lang"] == 'nl':
            print(tweetdict["id_str"], tweetdict["text"])
            
            sinfex_rslt = extractor.extract_traffic_inf([tweetdict["text"]])[0]
            
            if isinstance(sinfex_rslt, str): # this means there is not any match.
                #print("str sinfex result:",sinfex_rslt)
                print("\n\nNo Info:",tweetdict["text"])
            else:        
                tweetdict["sinfex_inf_str"], tweetdict["sinfex_inf_dicts"] = sinfex_rslt 
                tweetdict2 = self.del_keys_with_default(tweetdict)
                try:
                   print("\n\nAnnotated:"+tweetdict["sinfex_inf_str"])
                except Exception as ex:
                   logging.error("Tweet Error for the tweet : " + tweetdict["text"])
                   logging.error("Tweet Error Traceback : ", exc_info=True)
                   msg = "Tweet Error for the tweet :" + tweetdict["text"] + "\n\nError message : " + str(ex) + "\n\nSee the log file for more information about the error\n\nDate : " + (datetime.now()).strftime("%H:%M %d-%m-%y") 
                   print("Sinfex - Tweet Error", msg)
        
    def on_error(self, status_code):
        if status_code == 420:
            #returning False in on_data disconnects the stream
            print("Error 420")
            logging.error('Stream Error : Error 420', exc_info=True)
            print("Sinfex - Stream Error", "Error 420", "ebasar")
            #send_mail("Sinfex - Stream Error", "Error 420", "hurrial")
        else:
            print("Error code: ", status_code)
            logging.error('Stream Error : Error ' + str(status_code), exc_info=True)
            msg = "Error code: ", str(status_code)
            print("Sinfex - Stream Error", msg, "ebasar")
            #print("Sinfex - Stream Error", msg, "hurrial")
           
        return False

         
myauth = tweepy.OAuthHandler(config['twitterapp']['consumer_key'], config['twitterapp']['consumer_secret'])
myauth.set_access_token(config['twitterapp']['access_token'], config['twitterapp']['access_token_secret'])

print("Twitter authentication done.")

print("Streaming started")

myStreamListener = MyStreamListener()

#myStreamListener.set_notification(mynotified=False)

myStream = tweepy.streaming.Stream(auth = myauth, listener=myStreamListener)
myStream.filter(locations=[3.01,51.52,7.14,53.56])


# try:
#     myStreamListener = MyStreamListener()
#     myStream = tweepy.streaming.Stream(auth = myauth, listener=myStreamListener)
#     myStream.filter(locations=[3.01,51.52,7.14,53.56])
# except Exception as ex:
#     logging.error('Stream Failed : ', exc_info=True)
#     msg = "Stream Failed with the exception : " + str(ex) + "\n\nSee the log file for more information about the error\n\nDate : " + (datetime.now()).strftime("%H:%M %d-%m-%y") 
#     print("Sinfex - Stream Failed", msg, "ebasar")
#     #send_mail("Sinfex - Stream Failed", msg, "hurrial")

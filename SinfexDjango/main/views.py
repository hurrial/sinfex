
from django.shortcuts import render
from django.views.generic import View

from django.conf import settings

import io
import re
import folium
import logging
import configparser
from datetime import datetime, timedelta
from math import radians, cos, sin, asin, sqrt

import mongoengine
from mongoengine.queryset.visitor import Q
from pymongo import MongoClient

from main.models import SinInf, Feedbacks


logging.basicConfig(
		format='%(asctime)s, %(levelname)s: %(message)s',
		filename='data/sinfexdjango.log',
		datefmt='%d-%m-%Y, %H:%M',
		level=logging.INFO)


dateformat = "%d-%m-%Y"




####################  PyMongo Connection #################### 

config = configparser.ConfigParser()
config.read("data/sinfex_local.ini")

db_host = config.get('sinfex_mongo_db', 'client_host')
db_port = int(config.get('sinfex_mongo_db', 'client_port'))
db_name = config.get('sinfex_mongo_db', 'db_name')
sin_coll = config.get('sinfex_mongo_db', 'sin_coll')
feed_coll = config.get('sinfex_mongo_db', 'feed_coll')

client = MongoClient(db_host, db_port)
sin_coll = client[db_name][sin_coll]
feed_coll = client[db_name][feed_coll]



############################## FUNCTIONS FOR VIEWS ##############################


def call_sins(date, dt):
	'''This function calls types and the sinfex data belongs to that types'''

	dayends = date.replace(hour=23, minute=59, second=59, microsecond=59)
	daybegins = date.replace(hour=0, minute=0, second=0, microsecond=1)

	# In our db, 'created_at' field is a string formated like "Thu Dec 10 12:00:12 +0000 2015 "
	# So that, we convert the datetime into string in two different format looks like day("Thu Dec 10") and year("2015")
	# To find the data belongs to that date, we check if the first part of 'created_at' matches with day and the last part matches with year.
	day = datetime.strftime(date, "%a %b %d")
	year = datetime.strftime(date, "%Y")

	# Find the data according to given date information. Date is explained in above comments.
	sinlist = SinInf.objects(((Q(created_at__lte = dayends) & Q(created_at__gte = daybegins)) | (Q(created_at__startswith = day) & Q(created_at__endswith = year))) & Q(sinfex_inf_dicts__road_id__exists = True)).only(*['sinfex_inf_dicts', 'created_at', 'sinfex_inf_str'])

	# Find the different type names in the database;
	typelist = []
	for sin in sinlist:
		sintype = sin["sinfex_inf_dicts"][0]["type"]
		if(sintype not in typelist):
				typelist.append(sintype)

	# Find sinfex db items belongs to a type, collect them in a list(sinlistoftype) and append that list into another list(sinObjlist) to call all data for one type
	# Bir type'a ait butun database item'larini bulup bir listeye topluyor, sonra o listeleri bir baska listeye atiyoruz ki bir type'a karsilik gelen database item'larin listesine ulasabilelim.
	sinObjlist = []
	for t in typelist:
		sinlistoftype = []
		for sin in sinlist:
			if(sin["sinfex_inf_dicts"][0]["type"] == t and sin not in sinlistoftype):
					sinlistoftype.append(sin)
		sinObjlist.append(sinlistoftype)

	# Zip typelist with the list which has the sinfex data lists. By this method we can call both of the at the same time and print a data list belongs to single type.
	alltypeslist = [{'type': t[0], 'sinlistoftype': t[1]} for t in zip(typelist, sinObjlist)]

	nextDay = (date + timedelta(days=1)).strftime(dateformat)
	prevDay = (date + timedelta(days=-1)).strftime(dateformat)

	if(dt == datetime.strftime(datetime.now(), dateformat)):
		currdate = "Today"
	else:
		currdate =  datetime.strftime(date, "%A %B %d %Y")

	return alltypeslist, nextDay, prevDay, currdate



def create_map(sinlist):

		mymap = folium.Map(location=[52.4162188,5.6752709], zoom_start=8)

		for sin in sinlist:
			if(sin['coordinates'] is not None and sin['sinfex_inf_str'] is not None):
				if(type(sin['created_at']) == str):
					mymap.simple_marker(sin['coordinates']['coordinates'][::-1], popup=(sin['sinfex_inf_str'].split('Annotated TWEET:')[1] + "<br> Created at: "+sin['created_at'][11:19]))
				elif(type(sin['created_at']) == datetime):
					mymap.simple_marker(sin['coordinates']['coordinates'][::-1], popup=(sin['sinfex_inf_str'].split('Annotated TWEET:')[1] + "<br> Created at: "+datetime.strftime(sin['created_at'], '%H:%M:%S')))
				else:
					mymap.simple_marker(sin['coordinates']['coordinates'][::-1], popup=(sin['sinfex_inf_str'].split('Annotated TWEET:')[1] + "<br> Created at: Invalid Time Format"))
		mymap.create_map('templates/ifmap.html')

		# Add css file link to iframe code to color the types:

		with open('templates/ifmap.html', 'r') as file:
			data = file.readlines()

		data[25] = '\t<link rel="stylesheet" type="text/css" href="static/tags.css" />'

		with open('templates/ifmap.html', 'w') as file:
			file.writelines(data)

		return 0



def log_client_ip(request):
	ip = request.META.get('HTTP_CF_CONNECTING_IP')
	if ip is None:
		ip = request.META.get('REMOTE_ADDR')

	if(request.META.get('REMOTE_HOST')):
		userhost = request.META.get('REMOTE_HOST')
	else:
		userhost = 'unknown'

	logging.info('IP = ' + ip + ', Host = ' + userhost)



def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    km = 6367 * c
    return km




############################## VIEWS ##############################




class Home(View):

	def get(self, request):

		log_client_ip(request)

		today = datetime.now()
		dt = datetime.strftime(today, dateformat)

		alltypeslist, nextDay, prevDay, currdate = call_sins(today, dt)

		return render(request, 'base.html', {
				'date' : dt,		
				'alltypeslist' : alltypeslist,	
				'nextDay': nextDay,
				'prevDay': prevDay,	
				'currdate' : currdate,
		})


	def post(self, request):

		
		if "keysearch" in request.POST:

			keyword = request.POST['keyword']

			monthago = datetime.now() - timedelta(days=30)

			#sinlist = SinInf.objects(Q(created_at__gte = monthago) & Q(sinfex_inf_str__icontains = keyword)).only(*['created_at', 'sinfex_inf_str']) #mongoengine
			sinlist = list(sin_coll.find({'created_at': {'$lt': datetime.now(), '$gte': monthago}, 'sinfex_inf_str' : re.compile(r'\b' + keyword + r'\b', re.IGNORECASE)}, fields={'sinfex_inf_str': 1, 'created_at': 1}).sort('created_at', -1)) #pymongo

			for sin in sinlist:
				sin['created_time'] = datetime.strftime(sin['created_at'], '%d %B %Y %H:%M:%S') #'%A %B %d %Y %H:%M:%S')
				sin['objectid'] = sin['_id'] # '_id' can not be called from templates because of underscore
				# if we used mongoengine here (instead of pymongo), then calling the ObjectId with "id" would be ok because of the models(.py)

			return render(request, 'keysearch.html', {
				'date' : datetime.strftime(datetime.now(), dateformat),
				'sinlist' : sinlist,
				'keyword' : keyword,
			})


		elif "feedback" in request.POST:

			#zerolist = request.POST.getlist('0list')
			#onelist = request.POST.getlist('1list')
			#twolist = request.POST.getlist('2list')
			#threelist = request.POST.getlist('3list')
			#fourlist = request.POST.getlist('4list')
			#fivelist = request.POST.getlist('5list')

			vote = request.POST.getlist('vote');
			dt = request.POST['date']

			for v in vote:
				idvotelist = v.split('/')
				obj_id = idvotelist[0]
				vote = idvotelist[1]

				tweet_id = SinInf.objects.only('id_str').get(id = obj_id)['id_str']

				if (vote == '0'):
					Feedbacks.objects(tweet_id=tweet_id).update_one(inc__ratings__0=1, upsert=True)
				elif (vote == '1'):
					Feedbacks.objects(tweet_id=tweet_id).update_one(inc__ratings__1=1, upsert=True)
				elif (vote == '2'):
					Feedbacks.objects(tweet_id=tweet_id).update_one(inc__ratings__2=1, upsert=True)
				elif (vote == '3'):
					Feedbacks.objects(tweet_id=tweet_id).update_one(inc__ratings__3=1, upsert=True)
				elif (vote == '4'):
					Feedbacks.objects(tweet_id=tweet_id).update_one(inc__ratings__4=1, upsert=True)
				elif (vote == '5'):
					Feedbacks.objects(tweet_id=tweet_id).update_one(inc__ratings__5=1, upsert=True)


			date = datetime.strptime(dt, dateformat)

			alltypeslist, nextDay, prevDay, currdate = call_sins(date, dt)


			return render(request, 'base.html', {
				'alltypeslist' : alltypeslist,		
				'nextDay': nextDay,
				'prevDay': prevDay,	
				'currdate' : currdate,
				'date' : dt,
			})

			



class SinofDate(View):

	def get(self, request, dt):

		log_client_ip(request)

		date = datetime.strptime(dt, dateformat)

		alltypeslist, nextDay, prevDay, currdate = call_sins(date, dt)

		return render(request, "base.html", {
				'alltypeslist' : alltypeslist,		
				'nextDay': nextDay,
				'prevDay': prevDay,	
				'currdate' : currdate,
				'date' : dt,
		})



class RelTweets(View):

	def get(self, request, id):

		orgsin = SinInf.objects.get(pk=id)

		hourslater = orgsin['created_at'] + timedelta(hours=2)
		hoursago = orgsin['created_at'] - timedelta(hours=2)

		spatiotime = []

		if(orgsin['coordinates'] is not None):
			time2to2 = SinInf.objects(Q(created_at__lte = hourslater) & Q(created_at__gte = hoursago) & Q(sinfex_inf_str__exists = True) & Q(coordinates__exists = True)).only(*['created_at', 'sinfex_inf_str', 'coordinates'])
										#Q(coordinates__near=orgsin['coordinates']['coordinates'], coordinates__max_distance=100000)

			lonorgsin = orgsin['coordinates']['coordinates'][0]
			latorgsin = orgsin['coordinates']['coordinates'][1]

			spatiotime_temp = []

			for sin in time2to2:
				if(sin['coordinates'] is not None):
					lonsin = sin['coordinates']['coordinates'][0]
					latsin = sin['coordinates']['coordinates'][1]
					dist = haversine(lonorgsin, latorgsin, lonsin, latsin)
					if dist < 10:
						sin['dist'] = dist
						spatiotime_temp.append(sin)

			spatiotime = sorted(spatiotime_temp, key=lambda k: k['dist']) 


		return render(request, "reltweets.html", {
			'date' : datetime.strftime(datetime.now(), dateformat),
			'orgsin': orgsin,
			'spatiotime': spatiotime,
		})



class Map(View):

	def get(self, request, dt):

		date = datetime.strptime(dt, dateformat)

		dayends = date.replace(hour=23, minute=59, second=59, microsecond=59)
		daybegins = date.replace(hour=0, minute=0, second=0, microsecond=1)

		day = datetime.strftime(date, "%a %b %d")
		year = datetime.strftime(date, "%Y")

		sinlist = SinInf.objects(((Q(created_at__lte = dayends) & Q(created_at__gte = daybegins)) | (Q(created_at__startswith = day) & Q(created_at__endswith = year))) & Q(sinfex_inf_dicts__road_id__exists = True)).only(*['coordinates', 'created_at', 'sinfex_inf_str'])

		create_map(sinlist)

		nextDay = (date + timedelta(days=1)).strftime(dateformat)
		prevDay = (date + timedelta(days=-1)).strftime(dateformat)

		if(dt == datetime.strftime(datetime.now(), dateformat)):
			currdate = "Today"
		else:
			currdate =  datetime.strftime(date, "%A %B %d %Y")		

		return render(request, 'map.html', {
				'nextDay': nextDay,
				'prevDay': prevDay,	
				'currdate' : currdate,
				'date' : dt,	
		})



class IfMap(View): # view to call the map in iframe

	def get(self, request):

		return render(request, 'ifmap.html', {	
		})



class About(View):

	
	def get(self, request):


		return render(request, 'about.html', {	
				'date' : datetime.strftime(datetime.now(), dateformat),
		})





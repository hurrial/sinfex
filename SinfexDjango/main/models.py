
from django.db import models

from mongoengine import * # DynamicDocument, StringField

from datetime import datetime


class SinInf(DynamicDocument):

	meta = {
			'collection' : 'sinfex_inf',
			'ordering': ['-created_at'],
			#'indexes': [[('coordinates', '2dsphere')]]
			}
	sinfex_inf_dicts = DictField()
	created_at = StringField()
	sinfex_inf_str = StringField()
	coordinates = StringField() #PointField()
	is_quote_status = StringField()
	timestamp_ms = StringField()
	in_reply_to_status_id = StringField()
	entities = StringField()
	source = StringField()
	filter_level = StringField()
	retweeted = StringField()
	user = StringField()
	in_reply_to_user_id = StringField()
	in_reply_to_status_id_str = StringField()
	in_reply_to_screen_name = StringField()
	_id = StringField()
	contributors = StringField()
	lang = StringField()
	id = StringField()
	place = StringField()
	geo = StringField()
	truncated = StringField()
	retweet_count = StringField()
	in_reply_to_user_id_str = StringField()
	favorite_count = StringField()
	text = StringField()
	id_str = StringField()
	favorited = StringField()


	def created_time(self):
		ct = self.created_at
		if(type(ct) == str):
			ct1 = ct[11:19]
		elif(type(ct) == datetime):
			ct1 = datetime.strftime(ct, '%H:%M:%S')
		else:
			ct1 = 'Invalid Time Format'
		return ct1

	def coords(self):
		lon = self.coordinates['coordinates'][0]
		lat = self.coordinates['coordinates'][1]
		return lon, lat



class Feedbacks(DynamicDocument):

	meta = {'collection' : 'feedbacks'}
	tweet_id = StringField()
	ratings = DictField()





#!/bin/bash

ROOTDIR=/scratch2/www/Sinfex/SinfexDjango/

. /scratch2/www/Sinfex/envsinfex/bin/activate

#python3 manage.py runserver 3038
uwsgi --virtualenv $VIRTUAL_ENV --socket 127.0.0.1:3038 --chdir $ROOTDIR --wsgi-file $ROOTDIR/SinfexDjango/wsgi.py --logto $ROOTDIR/sinfexdjango.uwsgi.log --log-date --log-5xx --master --processes 4 --threads 2 --need-app --pidfile ./sinfex.pid

import os
import sys

sys.path.append('/scratch2/www/Sinfex/SinfexDjango/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "SinfexDjango.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

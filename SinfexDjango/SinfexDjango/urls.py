

from django.conf.urls import include, url

from main.views import *


urlpatterns = [
    
	url(r'^$', Home.as_view(), name='home'),

	url(r'^(?P<dt>\S+|\S*[^\w\s]\S*)/sinofdate$', SinofDate.as_view(), name='sinofdate'),

	url(r'^(?P<dt>\S+|\S*[^\w\s]\S*)/map$', Map.as_view(), name='map'),

	url(r'^(?P<id>\w+)/relevanttweets$', RelTweets.as_view(), name='reltweets'),

	url(r'^ifmap$', IfMap.as_view(), name='ifmap'), # url for iframe

	url(r'^about$', About.as_view(), name='about')

]
